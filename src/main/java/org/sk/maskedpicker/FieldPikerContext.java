package org.sk.maskedpicker;

import java.util.Locale;

import javax.swing.Action;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JComponent;

public class FieldPikerContext {

	private String _defaultValue;
	
	private Date _date;
	
	private Action _actionLookup;
	
	private Integer _horizontalAligment = 0;
	
	private Locale _locale = Locale.US;
	
	public String getDefaultValue() {
		return _defaultValue;
	}
	
	public void setDefaultValue(String defaultValue) {
		_defaultValue = defaultValue;
	}
	
	public Date getDate() {
		return _date;
	}
	
	public void setActionLookup(Action actionLookup) {
		this._actionLookup = actionLookup;
	}
	
	public Action getActionLookup() {
		return _actionLookup;
	}
	
	public void setHorizontalAligment(Integer horizontalAligment) {
		_horizontalAligment = horizontalAligment;
	}
	
	public Integer getHorizontalAligment() {
		return _horizontalAligment;
	}
	
	public void setLocale(Locale locale) {
		_locale = locale;
	}
	
	public Locale getLocale() {
		return _locale;
	}
}
