/**
 * $Id$
 * @author mcanovas
 * @date   Apr 20, 2012 9:57:41 AM
 *
 * Copyright (C) 2012 Scytl Secure Electronic Voting SA
 *
 * All rights reserved.
 *
 */
package org.sk.swing.fieldpiker;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.sk.maskedpicker.Calendar;
import org.sk.maskedpicker.Date;
import org.sk.maskedpicker.JFieldPickerDate;

/**
 * JUnit test for {@link JFieldPickerDate}.
 */
public class JFieldPickerDateTest {

    private JFieldPickerDate _jFieldPickerDate;

    private final Date _date = Calendar.createDate(1976, 11, 24);

    @Before
    public void before() {
        _jFieldPickerDate = new JFieldPickerDate();
    }

    @Test
    public void testGetDateEmpty() throws Exception {
        Assert.assertNull(_jFieldPickerDate.getDate());
    }

    @Test
    public void testJPanelCalendarDateDefault() throws Exception {
        Assert.assertEquals(new Date(), _jFieldPickerDate
            .getPanelCalendar().getCurrentDate());
    }

    @Test
    public void testSetDateToField() throws Exception {
        _jFieldPickerDate.setDate(_date);
        Assert.assertEquals(_jFieldPickerDate.getDate(), _date);
    }

    @Test
    public void testSetDateToCalendar() throws Exception {
        _jFieldPickerDate.setDate(_date);
        Assert.assertEquals(_jFieldPickerDate.getPanelCalendar()
            .getCurrentDate(), _date);
    }
}
