package org.sk.maskedpicker;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * String utilities extended from Apache Commons Lang.
 * 
 * @author Mario Cánovas
 */
public class StringUtils extends org.apache.commons.lang.StringUtils {
    /**
     * Separates with a blank the different tokens that can compose a normal
     * class or method name, like for instance doSomething into Do something.
     * 
     * @param str
     *            The string to separate.
     * @return The separated string.
     */
    public static String separeTokens(final String str) {
        StringBuffer b = new StringBuffer();
        if (str != null) {
            for (int i = 0; i < str.length(); i++) {
                if (i == 0) {
                    b.append(Character.toUpperCase(str.charAt(i)));
                } else {
                    if (Character.isLowerCase(str.charAt(i - 1))
                        && Character.isUpperCase(str.charAt(i))) {
                        b.append(' ');
                        if ((i < str.length() - 1)
                            && Character.isUpperCase(str.charAt(i + 1))) {
                            b.append(str.charAt(i));
                        } else {
                            b.append(Character.toLowerCase(str.charAt(i)));
                        }
                    } else {
                        b.append(str.charAt(i));
                    }
                }
            }
        }
        return b.toString();
    }

    /**
     * Parse a comma delimited "XX","XX","XX" string, and return an array with
     * its elements.
     * 
     * @param strToParse
     *            The String to parse.
     * @return An array of strings that are the parts of the comma delimited
     *         string to parse.
     */
    public static String[] parseCommaDelimitedString(String strToParse) {
        if (strToParse.length() == 0) {
            return new String[0];
        }
        int start = 0;
        while (strToParse.charAt(start) != '\"') {
            start++;
        }
        int end = strToParse.length() - 1;
        while (strToParse.charAt(end) != '\"') {
            end--;
        }
        strToParse = strToParse.substring(++start, end);
        ArrayList<String> list = new ArrayList<String>();
        start = 0;
        while (true) {
            end = strToParse.indexOf("\",\"", start);
            if (end == -1) {
                list.add(strToParse.substring(start));
                break;
            } else {
                list.add(strToParse.substring(start, end));
            }
            start = end + 3;
        }
        return list.toArray(new String[list.size()]);
    }

    /**
     * Utility to parse command line arguments in the form of arg=value. Returns
     * a map with the arguments as pairs of keys/values.
     * 
     * @param args
     *            The array of command line arguments.
     * @return A map with pairs of argument/value.
     */
    public static HashMap<String, String> parseArgs(final String[] args) {
        HashMap<String, String> map = new HashMap<String, String>();
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                String[] parts = split(args[i], "=");
                if (parts.length == 0) {
                    continue;
                }
                String arg = parts[0];
                String val = arg;
                if (parts.length > 1) {
                    val = parts[1];
                }
                map.put(arg, val);
            }
        }
        return map;
    }

    /**
     * Parses a string of the form "en-US,es-ES,fr,FR" or "en_US,es_ES,fr_FR"
     * into an array of locales.
     * 
     * @param str
     *            The argument string.
     * @return The parsed array of locales.
     */
    public static Locale[] parseLocales(final String str) {
        ArrayList<Locale> locales = new ArrayList<Locale>();
        String[] strLoc = split(str, ",");
        for (int i = 0; i < strLoc.length; i++) {
            Locale locale = parseLocale(strLoc[i]);
            if (locale == null) {
                continue;
            }
            locales.add(locale);
        }
        return locales.toArray(new Locale[locales.size()]);
    }

    /**
     * Parse a locale in the form "en-US" or "en_US"
     * 
     * @param str
     *            The locale string
     * @return The locale or null.
     */
    public static Locale parseLocale(final String str) {
        String[] parts = split(str.trim(), "_-");
        if (parts.length == 1) {
            return new Locale(parts[0].trim());
        }
        if (parts.length >= 2) {
            return new Locale(parts[0].trim(), parts[1].trim());
        }
        return null;
    }

    /**
     * Parse a comma separated list of strings "S1, S2, S3".
     * 
     * @param string
     *            The string to be tokenized.
     * @return the array of tokens.
     */
    public static String[] parseCommaSeparatedStrings(final String string) {
        StringTokenizer tokenizer = new StringTokenizer(string, ",");
        ArrayList<String> list = new ArrayList<String>();
        while (tokenizer.hasMoreTokens()) {
            list.add(tokenizer.nextToken().trim());
        }
        return list.toArray(new String[list.size()]);
    }

    /**
     * Converts a String to a new String using a selected charset.
     * 
     * @param in
     *            The input String
     * @param charset
     *            The charset to convert.
     * @return A String.
     * @throws UnsupportedEncodingException
     */
    public static String encodeString(final String in, final String charset)
            throws UnsupportedEncodingException {
        return new String(in.getBytes(charset));
    }

    /**
     * This method unescapes a message. It is usefull to decode the request
     * arguments in a HTTP Query String or a HTTP form URL encoded request.
     * 
     * @param msg
     *            The String to be unescaped.
     * @param charsetName
     *            The charset to be used.
     * @return the unescaped String
     * @throws UnsupportedEncodingException
     */
    public static String toUnescaped(final String msg,
            final String charsetName) throws UnsupportedEncodingException {
        ArrayList<Integer> list = new ArrayList<Integer>();
        for (int i = 0; i < msg.length(); i++) {
            char c = msg.charAt(i);
            if (c == '%') {
                String hex = msg.substring(i + 1, i + 3);
                list.add(Integer.valueOf(hex, 16));
                i += 2;
            } else {
                list.add(Integer.valueOf((byte) c));
            }
        }
        byte[] buff = new byte[list.size()];
        for (int i = 0; i < list.size(); i++) {
            buff[i] = list.get(i).byteValue();
        }
        return new String(buff, charsetName);
    }

    /**
     * This method unescapes a message. It is usefull to decode the request
     * arguments in a HTTP Query String or a HTTP form URL encoded request.
     * 
     * @param bytes
     *            The byte array to be unescaped.
     * @param charsetName
     *            The charset to be used.
     * @return the unescaped String
     * @throws UnsupportedEncodingException
     */
    public static String toUnescaped(final byte[] bytes,
            final String charsetName) throws UnsupportedEncodingException {
        return toUnescaped(new String(bytes), charsetName);
    }

    /**
     * This method escapes a message. It is usefull to encode the request
     * arguments in a HTTP GET request method.
     * 
     * @param msg
     *            The String to be escaped.
     * @param charsetName
     *            The charset to be used.
     * @return the escaped String
     * @throws UnsupportedEncodingException
     */
    public static String toEscaped(final String msg,
            final String charsetName) throws UnsupportedEncodingException {
        byte[] buff = msg.getBytes(charsetName);
        StringBuffer b = new StringBuffer();
        for (int i = 0; i < buff.length; i++) {
            int c = buff[i];
            if (((c >= '0') && (c <= '9')) || ((c >= 'A') && (c <= 'Z'))
                || ((c >= 'a') && (c < 'z'))) {
                b.append((char) c);
            } else {
                b.append('%');
                b.append(Integer.toHexString(c & 0xff).toUpperCase());
            }
        }
        return b.toString();
    }

    /**
     * Rellenar con espacios por la izquierda.
     * 
     * @return java.lang.String
     * @param source
     *            java.lang.String
     * @param padLength
     *            int
     */
    public static String padLeft(final String source, final int padLength) {
        return padLeft(source, padLength, '\u0020');
    }

    /**
     * Rellenar con el car�cter especificado por la izquierda.
     * 
     * @return java.lang.String
     * @param source
     *            java.lang.String
     * @param padLength
     *            int
     * @param padChar
     *            char
     */
    public static String padLeft(final String source, final int padLength,
            final char padChar) {
        char[] chrArr = new char[padLength + source.length()];
        for (int i = 0; i < padLength; i++) {
            chrArr[i] = padChar;
        }
        source.getChars(0, source.length(), chrArr, padLength);
        return new String(chrArr, source.length(), padLength);
    }

    /**
     * Rellenar con espacios por la derecha.
     * 
     * @return java.lang.String
     * @param source
     *            java.lang.String
     * @param padLength
     *            int
     */
    public static String padRight(final String source, final int padLength) {
        return padRight(source, padLength, '\u0020');
    }

    /**
     * Rellenar con el car�cter especificado por la derecha.
     * 
     * @return java.lang.String
     * @param source
     *            java.lang.String
     * @param padLength
     *            int
     * @param padChar
     *            char
     */
    public static String padRight(final String source,
            final int padLength, final char padChar) {
        char[] chrArr = new char[source.length() + padLength];
        for (int i = source.length(); i < chrArr.length; i++) {
            chrArr[i] = padChar;
        }
        source.getChars(0, source.length(), chrArr, 0);
        return new String(chrArr, 0, padLength);
    }

    /**
     * Eliminar los espacios por la izquierda.
     * 
     * @return java.lang.String
     * @param str
     *            java.lang.String
     */
    public static String trimLeft(final String str) {
        if (str.length() == 0) {
            return str;
        }
        int len = str.length();
        int start = 0;
        while (start < len && str.charAt(start) == ' ') {
            start++;
        }
        return str.substring(start);
    }

    /**
     * Eliminar los espacios por la derecha.
     * 
     * @return java.lang.String
     * @param str
     *            java.lang.String
     */
    public static String trimRight(final String str) {
        if (str.length() == 0) {
            return str;
        }
        int len = str.length();
        while (len > 0 && str.charAt(len - 1) == ' ') {
            len--;
        }
        return str.substring(0, len);
    }

    /**
     * Devuelve un array de objetos <b>String</b>, extra�dos del primer
     * argumento utilizando como separador el segundo argumento.
     * 
     * @return java.lang.String[]
     * @param string
     *            java.lang.String
     * @param separator
     *            java.lang.String
     */
    public static String[] parse(final String string,
            final String separator) {
        return parse(string, separator, true);
    }

    /**
     * Devuelve un array de objetos <b>String</b>, extra�dos del primer
     * argumento utilizando como separador el segundo argumento.
     * 
     * @return java.lang.String[]
     * @param string
     *            java.lang.String
     * @param separator
     *            java.lang.String
     * @param emptyLines
     *            boolean
     */
    public static String[] parse(final String string,
            final String separator, final boolean emptyLines) {

        Vector<String> stringList = new Vector<String>();
        int index = 0;
        while (index >= 0) {
            int tempIndex = string.indexOf(separator, index);
            String tempString = null;
            if (tempIndex >= 0) {
                tempString = string.substring(index, tempIndex);
            } else {
                tempString = string.substring(index);
            }
            if (emptyLines || tempString.trim().length() > 0) {
                stringList.addElement(tempString);
            }
            index =
                (tempIndex == -1 ? -1 : tempIndex + separator.length());
        }

        String[] stringArray = new String[stringList.size()];
        for (int n = 0; n < stringArray.length; n++) {
            stringArray[n] = stringList.elementAt(n);
        }

        return stringArray;
    }

    /**
     * Sustituir una cadena por otra.
     * 
     * @return java.lang.String
     * @param source
     *            java.lang.String
     * @param search
     *            java.lang.String
     * @param replace
     *            java.lang.String
     */
    public static String replace(final String source, final String search,
            final String replace) {
        int start = 0;
        int index = source.indexOf(search, start);
        if (index == -1) {
            return source;
        }
        StringBuffer buffer = new StringBuffer(source.length() * 2);
        while (index != -1) {
            buffer.append(source.substring(start, index) + replace);
            start = index + search.length();
            index = source.indexOf(search, start);
        }
        if (start < source.length()) {
            buffer.append(source.substring(start));
        }
        return buffer.toString();
    }

    /**
     * Repetir un car�cter.
     * 
     * @return java.lang.String
     * @param chr
     *            char
     * @param count
     *            int
     */
    public static String replicate(final char chr, final int count) {
        char[] chars = new char[count];
        for (int i = 0; i < count; i++) {
            chars[i] = chr;
        }
        return new String(chars);
    }

    /**
     * Quita los espacioa en blanco por delante y por detras
     * 
     * @param string
     * @return la cadena sin espacios por delante o per detras
     */
    public static String alltrim(String string) {
        if (string == null) {
            string = "";
        }
        return trimRight(trimLeft(string));
    }
}
