package org.sk.maskedpicker;

import java.awt.Component;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Icon;
import javax.swing.KeyStroke;

/**
 * An extended action that has the following additional properties:
 * <ul>
 * <li>It can get its name and short description from resources in the default
 * text server.</li>
 * <li>It publishes a list of string access keys.</li>
 * <li>It can be tagged to be hidden for a list of edit modes (EditMode.class)</li>
 * </ul>
 * 
 * @author Mario Cánovas
 */
public abstract class ActionBase extends AbstractAction {

    private static final long serialVersionUID = 1L;

    /** Action id. This is the main identifier of the action. */
    public final static String ID = "ID";

    /** The string key for the name. */
    public final static String NAME_KEY = "NAME_KEY";

    /** The string key for the short description. */
    public final static String SHORT_DESCRIPTION_KEY =
        "SHORT_DESCRIPTION_KEY";

    /** The string key for the action group. */
    public final static String ACTION_GROUP = "ACTION_GROUP";

    /** The string key to sort the action in its action group. */
    public final static String SORT_KEY = "SORT_KEY";

    /** The string key for the edit mode property. */
    public final static String EDIT_MODE = "EDIT_MODE";

    /** The string key for access keys published by this action. */
    public final static String ACCESS_KEY = "ACCESS_KEY";

    /** The string key for the hidden edit modes. */
    public final static String HIDDEN_EDIT_MODES = "HIDDEN_EDIT_MODES";

    /** The string key to hide this action in the buttons bar. */
    public final static String HIDDEN_IN_BUTTONS_BAR =
        "HIDDEN_IN_BUTTONS_BAR";

    /** The string key to hide this action in the popup menu. */
    public final static String HIDDEN_IN_POPUP_MENU =
        "HIDDEN_IN_POPUP_MENU";

    /** Std access key for actions launched from the main menu. */
    public final static String EXECUTE_ACTION = "EXECUTE";

    /** Working session. */
    public final static String WORKING_SESSION = "WORKING_SESSION";

    protected transient ActionEvent currentActionEvent = null;

    /**
     * Constructor assigning the action name.
     * 
     * @param session
     *            The current session.
     * @param name
     *            The action name.
     */
    public ActionBase(final String name) {
        super(name);
    }

    /**
     * Constructor assigning the action name and icon.
     * 
     * @param session
     *            The current session.
     * @param name
     *            The action name.
     * @param icon
     *            The action icon.
     */
    public ActionBase(final String name, final Icon icon) {
        super(name, icon);
    }

    /**
     * Check if an access string is contained in the this action list of access
     * strings.
     * 
     * @param string
     *            The access string to check.
     * @return A boolean.
     */
    public boolean containsAccessString(final String string) {
        ArrayList<String> list = (ArrayList<String>) getValue(ACCESS_KEY);
        if (list == null) {
            return false;
        }
        return list.contains(string);
    }

    /**
     * Removes the access string from the list of published strings.
     * 
     * @param string
     *            The access string to remove.
     */
    public void removeAccessString(final String string) {
        ArrayList<String> list = (ArrayList<String>) getValue(ACCESS_KEY);
        if (list == null) {
            return;
        }
        list.remove(string);
    }

    /**
     * Returns the accelerator key stroke.
     * 
     * @return The accelerator key stroke.
     */
    public KeyStroke getAcceleratorKey() {
        return (KeyStroke) getValue(ACCELERATOR_KEY);
    }

    /**
     * Get the string key of the name.
     * 
     * @return The string key of the name.
     */
    public String getKeyName() {
        return (String) getValue(NAME_KEY);
    }

    /**
     * Set the string key of the name.
     * 
     * @param key
     *            The string key of the name.
     */
    public void setKeyName(final String key) {
        putValue(NAME_KEY, key);
    }

    /**
     * Get the string key of the short description.
     * 
     * @return The string key of the short description.
     */
    public String getKeyShortDescription() {
        return (String) getValue(SHORT_DESCRIPTION_KEY);
    }

    /**
     * Set the string key of the short description.
     * 
     * @param key
     *            The string key of the short description.
     */
    public void setKeyShortDescription(final String key) {
        putValue(SHORT_DESCRIPTION_KEY, key);
    }

    /**
     * Return this action name.
     * 
     * @return The name.
     */
    public String getName() {
        return (String) getValue(NAME);
    }

    /**
     * Returns the key to sort the action in its action group.
     * 
     * @return The sort key
     */
    public String getSortKey() {
        String sortKey = (String) getValue(SORT_KEY);
        return (sortKey == null ? "" : sortKey);
    }

    /**
     * Return this action short description.
     * 
     * @return The short description.
     */
    public String getShortDescription() {
        return (String) getValue(SHORT_DESCRIPTION);
    }

    /**
     * Set an accelerator key.
     * 
     * @param key
     *            The accelerator key.
     */
    public void setAcceleratorKey(final KeyStroke key) {
        putValue(ACCELERATOR_KEY, key);
    }

    /**
     * Sets the sort key to sort the action within its action group.
     * 
     * @param key
     *            The sort key
     */
    public void setSortKey(final String key) {
        putValue(SORT_KEY, key);
    }

    /**
     * Set an accelerator key.
     * 
     * @param keyCode
     *            The accelerator key code.
     * @param modifiers
     *            The accelerator key modifiers.
     */
    public void setAcceleratorKey(final int keyCode, final int modifiers) {
        putValue(ACCELERATOR_KEY,
            KeyStroke.getKeyStroke(keyCode, modifiers));
    }

    /**
     * Set an accelerator key.
     * 
     * @param keyCode
     *            The accelerator key code.
     */
    public void setAcceleratorKey(final int keyCode) {
        putValue(ACCELERATOR_KEY, KeyStroke.getKeyStroke(keyCode, 0));
    }

    /**
     * Sets the icon.
     * 
     * @param icon
     *            The icon to set.
     */
    public void setIcon(final Icon icon) {
        putValue(SMALL_ICON, icon);
    }

    /**
     * Gets the icon.
     * 
     * @return The icon.
     */
    public Icon getIcon() {
        return (Icon) getValue(SMALL_ICON);
    }

    /**
     * Executes this action. This method must to be abstract.
     * 
     * @param e
     *            The action event that promoted this execution.
     * @throws Exception
     */
    protected abstract void execute(ActionEvent e) throws Exception;

    /**
     * Tries to execute this action. <br>
     * This method can be overridden to catch exceptions of the action executed.
     * 
     * @param e
     *            The origin event.
     */
    protected void tryExecute(final ActionEvent e) {
        try {
            execute(e);
        } catch (Exception e1) {
            setCursorDefault();
            if (e1 instanceof RuntimeException) {
                throw (RuntimeException) e1;
            } else {
                throw new RuntimeException(e1);
            }
        }
    }

    /**
     * Invoked when an action occurs. This method is called to invoke the
     * action, and must not be extended.
     * 
     * @see #execute(ActionEvent)
     */
    public final void actionPerformed(final ActionEvent e) {
        try {
            currentActionEvent = e;
            setCursorBusy();
            tryExecute(e);
        } finally {
            setCursorDefault();
        }
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return getName();
    }

    /*
     * (non-Javadoc)
     * @see java.lang.Object#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (!(obj instanceof ActionBase)) {
            return false;
        }
        ActionBase action = (ActionBase) obj;
        return getId().equals(action.getId());
    }

    /**
     * Gets the action identifier. When is not defined, the class name is
     * returned.
     * 
     * @return The id.
     */
    public String getId() {
        String id = (String) getValue(ID);
        if (id == null) {
            id = getClass().getName();
            setId(id);
        }
        return id;
    }

    /**
     * Sets the action identifier.
     * 
     * @param id
     *            The id to set.
     */
    public void setId(final String id) {
        putValue(ID, id);
    }

    /**
     * GUI Dependent. <br>
     * Sets the cursor to busy.
     */
    protected void setCursorBusy() {
        if (currentActionEvent == null) {
            return;
        }
        // Set the cursor to busy.
        if (currentActionEvent.getSource() instanceof Component) {
            Component component =
                (Component) currentActionEvent.getSource();
            component = SwingUtil.getFirstParentFrameOrDialog(component);
            if (component != null) {
                component.setCursor(Cursor
                    .getPredefinedCursor(Cursor.WAIT_CURSOR));
            }
        }
    }

    /**
     * GUI Dependent. <br>
     * Sets the cursor to default.
     */
    protected void setCursorDefault() {
        if (currentActionEvent == null) {
            return;
        }
        // Set the cursor to busy.
        if (currentActionEvent.getSource() instanceof Component) {
            Component component =
                (Component) currentActionEvent.getSource();
            component = SwingUtil.getFirstParentFrameOrDialog(component);
            if (component != null) {
                component.setCursor(Cursor.getDefaultCursor());
            }
        }
    }

}
