package org.sk.maskedpicker;

/**
 * A <code>Timestamp</code> that comforms with the contract of hash code. Note
 * that the superclass <code>java.sql.Timestamp</code> does not. It's usefull to
 * have values used directly as keys in hash maps.
 * 
 * @author Mario Cánovas
 */
public class Timestamp extends java.sql.Timestamp {
    /**
     * Generated serial version id.
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default constructor.
     */
    public Timestamp() {
        super(System.currentTimeMillis());
    }

    /**
     * Creates a new instance of Timestamp.
     * 
     * @param time
     *            The time in milliseconds.
     */
    public Timestamp(final long time) {
        super(time);
    }

    /**
     * Copy constructor.
     * 
     * @param time
     *            The timestamp to be copied.
     */
    public Timestamp(final java.sql.Timestamp time) {
        super(time.getTime());
    }

    /**
     * Returns the hash code for this value.
     * <p>
     * 
     * @return The hash code
     */
    @Override
    public int hashCode() {
        long ht = getTime() + getNanos() / 1000000;
        return (int) ht ^ (int) (ht >> 32);
    }

    /**
     * Compares this timestamp with the argument timestamp. Returns 0 if they
     * are equal, -1 if this value is less than the argument, and 1 if it is
     * greater.
     * <p>
     * 
     * @return An <code>int</code>.
     * @param t
     *            The <code>Timestamp</code> to compare with.
     */
    public int compareTo(final Timestamp t) {
        int compare = super.compareTo(t);
        if (compare == 0) {
            int n1 = getNanos();
            int n2 = t.getNanos();
            compare = (n1 == n2 ? 0 : (n1 < n2 ? -1 : 1));
        }
        return compare;
    }
}
