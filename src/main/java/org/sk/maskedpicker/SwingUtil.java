package org.sk.maskedpicker;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.InputMethodListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.AbstractButton;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

/**
 * Utility class to work with swing components.
 * 
 * @author Mario Cánovas
 */
public class SwingUtil {

    /**
     * Default text field height.
     */
    public final static int DEFAULT_TEXTFIELD_HEIGHT = 18;

    /**
     * Default frame width factor.
     */
    public final static double DEFAULT_FRAME_WIDTH_FACTOR = 0.9;

    /**
     * Default frame height factor.
     */
    public final static double DEFAULT_FRAME_HEIGHT_FACTOR = 0.9;

    /**
     * The maximum number of rows to show in a table view.
     */
    public final static int MAX_ROWS_IN_TABLE_VIEW = 10000;

    /**
     * The current window, dialog or frame, that is, the window that has focus.
     */
    private static Component currentWindow = null;

    /**
     * Default insets.
     */
    private static Insets defaultInsets = new Insets(1, 2, 1, 2);

    /**
     * Default vertical gap.
     */
    private static int defaultVerticalGap = 2;

    /**
     * Default horizontal gap.
     */
    private static int defaultHorizontalGap = 2;

    /**
     * Package private setter for the current window.
     * 
     * @param window
     *            The window to set as the current window.
     */
    static void setCurrentWindow(final Component window) {
        currentWindow = window;
    }

    /**
     * Returns the window that has focus.
     * 
     * @return The current window, the one that has focus.
     */
    public static Component getCurrentWindow() {
        return currentWindow;
    }

    /**
     * Close a top Frame or Dialog
     * 
     * @param cmp
     *            Any component contained in the frame or dialog.
     */
    public static void closeFrameOrDialog(final Component cmp) {
        // Get the top component and check it is a JFrame
        Component topComponent =
            SwingUtil.getFirstParentFrameOrDialog(cmp);
        boolean frame = true;
        boolean dialog = true;
        if (!(topComponent instanceof JFrame)) {
            frame = false;
        }
        if (!(topComponent instanceof JDialog)) {
            dialog = false;
        }
        if (!frame && !dialog) {
            throw new IllegalArgumentException(
                "Top component is not a JFrame or a JDialog");
        }
        // Cast and operate on the form
        if (frame) {
            JFrame frm = (JFrame) topComponent;
            frm.setVisible(false);
            frm.dispose();
        }
        if (dialog) {
            JDialog dlg = (JDialog) topComponent;
            dlg.setVisible(false);
            dlg.dispose();
        }
    }

    /**
     * Returns the <code>Point</code> to center a <code>Dimension</code> on the
     * screen.
     * 
     * @return The top left <code>Point</code>.
     * @param sz
     *            The <code>Dimension</code> to center.
     */
    public static Point centerDimensionOnScreen(final Dimension sz) {
        Dimension szScreen = getScreenSize();
        Point pt = new Point(0, 0);
        if (szScreen.width > sz.width) {
            pt.x = (szScreen.width - sz.width) / 2;
        }
        if (szScreen.height > sz.height) {
            pt.y = (szScreen.height - sz.height) / 2;
        }
        return pt;
    }

    /**
     * Locates a dimension on the screen being the left space the width factor
     * of difference between the screen size and the dimension. A value of 0
     * moves the dimension to the left, while a value of 1 moves the dimension
     * to the right. The same applies to the heigh.
     * 
     * @param sz
     *            The dimension to locate.
     * @param factorWidth
     *            Width factor.
     * @param factorHeight
     *            Height factor.
     * @return The top-left corner point.
     */
    public static Point moveDimensionOnScreen(final Dimension sz,
            final double factorWidth, final double factorHeight) {
        Dimension szScreen = getScreenSize();
        Point pt = new Point(0, 0);
        if (szScreen.width > sz.width) {
            pt.x = (int) ((szScreen.width - sz.width) * factorWidth);
            pt.y = (int) ((szScreen.height - sz.height) * factorHeight);
        }
        return pt;
    }

    /**
     * Returns a <code>Dimension</code> which width and height are a factor of
     * the screen width and height.
     * 
     * @return A <code>Dimension</code>.
     * @param factorWidth
     *            A factor for height 0 < factor <= 1
     * @param factorHeight
     *            A factor for height 0 < factor <= 1
     */
    public static Dimension factorScreenDimension(
            final double factorWidth, final double factorHeight) {
        Dimension d = getScreenSize();
        d.width *= factorWidth;
        d.height *= factorHeight;
        return d;
    }

    /**
     * Returns the array of all components contained in a component and its
     * subcomponents.
     * 
     * @param parent
     *            The parent component.
     * @param clazz
     *            The class to filter components.
     * @return The array of components.
     */
    public static Component[] getAllComponents(final Component parent,
            final Class<?> clazz) {
        return getAllComponents(parent, new Class[] {clazz });
    }

    /**
     * Returns the array of all components contained in a component and its
     * subcomponents.
     * 
     * @param parent
     *            The parent component.
     * @param classes
     *            an array of possible classes.
     * @return The array of components.
     */
    public static Component[] getAllComponents(final Component parent,
            final Class<?>[] classes) {
        ArrayList<Component> list = new ArrayList<Component>();
        Component[] components = getAllComponents(parent);
        for (int i = 0; i < components.length; i++) {
            for (int j = 0; j < classes.length; j++) {
                if (classes[j].isInstance(components[i])) {
                    list.add(components[i]);
                }
            }
        }
        return list.toArray(new Component[list.size()]);
    }

    /**
     * Returns the array of all components contained in a component and its
     * subcomponents.
     * 
     * @return The array of components.
     * @param parent
     *            The parent component.
     */
    public static Component[] getAllComponents(final Component parent) {
        ArrayList<Component> list = new ArrayList<Component>();
        fillComponentList(parent, list);
        return list.toArray(new Component[list.size()]);
    }

    /**
     * Returns the auto-resize mode.
     * 
     * @param mode
     *            The name of the mode.
     * @return The JTable mode.
     */
    public static int getAutoResizeMode(final String mode) {
        if (mode.equals("AUTO_RESIZE_ALL_COLUMNS")) {
            return JTable.AUTO_RESIZE_ALL_COLUMNS;
        } else if (mode.equals("AUTO_RESIZE_LAST_COLUMN")) {
            return JTable.AUTO_RESIZE_LAST_COLUMN;
        } else if (mode.equals("AUTO_RESIZE_NEXT_COLUMN")) {
            return JTable.AUTO_RESIZE_NEXT_COLUMN;
        } else if (mode.equals("AUTO_RESIZE_OFF")) {
            return JTable.AUTO_RESIZE_OFF;
        } else if (mode.equals("AUTO_RESIZE_SUBSEQUENT_COLUMNS")) {
            return JTable.AUTO_RESIZE_SUBSEQUENT_COLUMNS;
        } else {
            return JTable.AUTO_RESIZE_OFF;
        }
    }

    /**
     * Return the first parent that is a <code>JFrame</code> or a
     * <code>JDialog</code>.
     * 
     * @return The parent frame or dialog.
     * @param cmp
     *            The start search component.
     */
    public static Component getFirstParentFrameOrDialog(Component cmp) {
        while (cmp != null) {
            if (cmp instanceof JFrame || cmp instanceof JDialog) {
                return cmp;
            }
            cmp = cmp.getParent();
        }
        return null;
    }

    /**
     * Returns the firt parent component of the argument class.
     * 
     * @param cmp
     *            The starting component.
     * @param parentClass
     *            The desired parent class.
     * @return The first parent component that is of the argument class.
     */
    public static Component getFirstParent(Component cmp,
            final Class<?> parentClass) {
        while (cmp != null) {
            if (parentClass.isInstance(cmp)) {
                return cmp;
            }
            cmp = cmp.getParent();
        }
        return null;
    }

    /**
     * Returns the top most parent component of the argument class contained
     * inside the top most class.
     * 
     * @param component
     *            The starting component.
     * @param parentClass
     *            The desired parent class.
     * @param topMostClass
     *            The top most class.
     * @return The first parent component that is of the argument class.
     */
    public static Component getTopMostParent(final Component component,
            final Class<?> parentClass, final Class<?> topMostClass) {
        Component cmp = component;
        Component desired = null;
        while (cmp != null) {
            if (parentClass.isInstance(cmp)) {
                desired = cmp;
            }
            if (topMostClass != null && topMostClass.isInstance(cmp)) {
                return desired;
            }
            cmp = cmp.getParent();
        }
        return null;
    }

    /**
     * Returns the first child component of the argument class.
     * 
     * @param parent
     *            The starting component.
     * @param childClass
     *            The desired child class.
     * @return The first parent component that is of the argument class.
     */
    public static Component getFirstChild(final Container parent,
            final Class<?> childClass) {
        if (childClass.isInstance(parent)) {
            return parent;
        }
        for (int i = 0; i < parent.getComponentCount(); i++) {
            Component child = parent.getComponent(i);
            if (childClass.isInstance(child)) {
                return child;
            }
        }
        for (int i = 0; i < parent.getComponentCount(); i++) {
            Component child = parent.getComponent(i);
            if (child instanceof Container) {
                Component child2 =
                    getFirstChild((Container) child, childClass);
                if (child2 != null) {
                    return child2;
                }
            }
        }
        return null;
    }

    /**
     * Gets the screen size.
     * 
     * @return The screen size.
     */
    public static Dimension getScreenSize() {
        return Toolkit.getDefaultToolkit().getScreenSize();
    }

    /**
     * Returns the top component from a component.
     * 
     * @return The top component.
     * @param cmp
     *            A component.
     */
    public static Component getTopComponent(Component cmp) {
        while (cmp.getParent() != null) {
            cmp = cmp.getParent();
        }
        return cmp;
    }

    /**
     * Fills the array list with the all the components contained in the parent
     * component and its sub-components.
     * 
     * @param list
     *            An <code>ArrayList</code>.
     * @param cmp
     *            The parent component.
     */
    public static void fillComponentList(final Component cmp,
            final ArrayList<Component> list) {
        list.add(cmp);
        if (cmp instanceof Container) {
            Container cnt = (Container) cmp;
            for (int i = 0; i < cnt.getComponentCount(); i++) {
                fillComponentList(cnt.getComponent(i), list);
            }
        }
    }

    /**
     * Find a named component.
     * 
     * @param cmp
     *            The parent component.
     * @param name
     *            The name of the component to find.
     * @return The component or null if not found.
     */
    public static Component findComponent(Component cmp, final String name) {
        if (StringUtils.defaultString(cmp.getName()).equals(name)) {
            return cmp;
        }
        if (cmp instanceof Container) {
            Container cnt = (Container) cmp;
            for (int i = 0; i < cnt.getComponentCount(); i++) {
                cmp = findComponent(cnt, name);
                if (cmp != null) {
                    return cmp;
                }
            }
        }
        return null;
    }

    /**
     * returns an array with the buttons actions.
     * 
     * @param buttons
     *            An array of buttons.
     * @return An array of actions.
     */
    public static Action[] getActions(final JButton[] buttons) {
        Action[] actions = new Action[buttons.length];
        for (int i = 0; i < actions.length; i++) {
            actions[i] = buttons[i].getAction();
        }
        return actions;
    }

    /**
     * Sets the mnemonics for an array of actions or buttons. Starts with the
     * first character of the text, and if the mmnemonic is used, scans the text
     * for the first character not used.
     * 
     * @param objects
     *            The array of buttons to set the menmonics.
     */
    public static void setMnemonics(final Object[] objects) {
        HashMap<Character, Character> mnemonicMap =
            new HashMap<Character, Character>();
        for (int i = 0; i < objects.length; i++) {
            String text = setMnemonicsGetText(objects[i]);
            if (text != null && text.length() > 0) {
                int index = 0;
                char mnemonic = text.charAt(index);
                char uppercase = Character.toUpperCase(mnemonic);
                while (mnemonicMap.containsKey(uppercase)) {
                    index++;
                    if (index >= 0 && index < text.length()
                        && text.charAt(index) != ' ') {
                        mnemonic = text.charAt(index);
                        uppercase = Character.toUpperCase(mnemonic);
                    } else {
                        if (setMnemonicsGetMnemonic(mnemonic) > 0) {
                            break;
                        }
                    }
                }
                if (!mnemonicMap.containsKey(uppercase)) {
                    mnemonicMap.put(uppercase, uppercase);
                    setMnemonicsSetMnemonic(objects[i],
                        setMnemonicsGetMnemonic(mnemonic));
                }
            }
        }
    }

    private static String setMnemonicsGetText(final Object object) {
        if (object instanceof JButton) {
            return ((JButton) object).getText();
        }
        if (object instanceof Action) {
            return (String) ((Action) object).getValue(Action.NAME);
        }
        return null;
    }

    private static int setMnemonicsGetMnemonic(final char c) {
        int mnemonic = 0;
        switch (Character.toUpperCase(c)) {
        case '0':
            mnemonic = KeyEvent.VK_0;
            break;
        case '1':
            mnemonic = KeyEvent.VK_1;
            break;
        case '2':
            mnemonic = KeyEvent.VK_2;
            break;
        case '3':
            mnemonic = KeyEvent.VK_3;
            break;
        case '4':
            mnemonic = KeyEvent.VK_4;
            break;
        case '5':
            mnemonic = KeyEvent.VK_5;
            break;
        case '6':
            mnemonic = KeyEvent.VK_6;
            break;
        case '7':
            mnemonic = KeyEvent.VK_7;
            break;
        case '8':
            mnemonic = KeyEvent.VK_8;
            break;
        case '9':
            mnemonic = KeyEvent.VK_9;
            break;
        case 'A':
            mnemonic = KeyEvent.VK_A;
            break;
        case 'B':
            mnemonic = KeyEvent.VK_B;
            break;
        case 'C':
            mnemonic = KeyEvent.VK_C;
            break;
        case 'D':
            mnemonic = KeyEvent.VK_D;
            break;
        case 'E':
            mnemonic = KeyEvent.VK_E;
            break;
        case 'F':
            mnemonic = KeyEvent.VK_F;
            break;
        case 'G':
            mnemonic = KeyEvent.VK_G;
            break;
        case 'H':
            mnemonic = KeyEvent.VK_H;
            break;
        case 'I':
            mnemonic = KeyEvent.VK_I;
            break;
        case 'J':
            mnemonic = KeyEvent.VK_J;
            break;
        case 'K':
            mnemonic = KeyEvent.VK_K;
            break;
        case 'L':
            mnemonic = KeyEvent.VK_L;
            break;
        case 'M':
            mnemonic = KeyEvent.VK_M;
            break;
        case 'N':
            mnemonic = KeyEvent.VK_N;
            break;
        case 'O':
            mnemonic = KeyEvent.VK_O;
            break;
        case 'P':
            mnemonic = KeyEvent.VK_P;
            break;
        case 'Q':
            mnemonic = KeyEvent.VK_Q;
            break;
        case 'R':
            mnemonic = KeyEvent.VK_R;
            break;
        case 'S':
            mnemonic = KeyEvent.VK_S;
            break;
        case 'T':
            mnemonic = KeyEvent.VK_T;
            break;
        case 'U':
            mnemonic = KeyEvent.VK_U;
            break;
        case 'V':
            mnemonic = KeyEvent.VK_V;
            break;
        case 'W':
            mnemonic = KeyEvent.VK_W;
            break;
        case 'X':
            mnemonic = KeyEvent.VK_X;
            break;
        case 'Y':
            mnemonic = KeyEvent.VK_Y;
            break;
        case 'Z':
            mnemonic = KeyEvent.VK_Z;
            break;
        }
        return mnemonic;
    }

    private static void setMnemonicsSetMnemonic(final Object object,
            final int mnemonic) {
        if (object instanceof JButton) {
            ((JButton) object).setMnemonic(mnemonic);
        }
        if (object instanceof Action) {
            ((Action) object).putValue(Action.MNEMONIC_KEY, new Integer(
                mnemonic));
            ;
        }
    }

    /**
     * Returns an array with all JButton components contained in a top
     * component.
     * 
     * @param topComponent
     *            The top component to scan.
     * @return An array with all JButton components.
     */
    public static JButton[] getAllButtons(final Component topComponent) {
        Component[] components = getAllComponents(topComponent);
        ArrayList<JButton> buttons = new ArrayList<JButton>();
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof JButton) {
                buttons.add((JButton) components[i]);
            }
        }
        return buttons.toArray(new JButton[buttons.size()]);
    }

    /**
     * Gets a specified button from a container using its action.
     * 
     * @param topComponent
     *            The top component to scan.
     * @param action
     *            The action class of this button.
     * @return the button.
     */
    public static JButton getButton(final Component topComponent,
            final Class<? extends Action> action) {
        JButton[] buttons = getAllButtons(topComponent);
        for (JButton button : buttons) {
            if (action.isInstance(button.getAction())) {
                return button;
            }
        }
        return null;
    }

    /**
     * Separe a dimension from a point: usefull for popup menus or windows
     * 
     * @return The top-left point of the dimension.
     * @param sz
     *            The dimension.
     * @param pt
     *            The reference point.
     */
    public static Point separeDimensionFromPoint(final Dimension sz,
            final Point pt) {
        Dimension szScreen = getScreenSize();
        if (pt.x > szScreen.width / 2) {
            if (pt.x > sz.width) {
                pt.x = ((pt.x - sz.width) / 2);
            } else {
                pt.x = 0;
            }
        } else {
            int w = szScreen.width - pt.x;
            if (w > sz.width) {
                pt.x = (szScreen.width / 2) + ((w - sz.width) / 2);
            } else {
                pt.x = szScreen.width - sz.width;
            }
        }
        if (szScreen.height > sz.height) {
            pt.y = (szScreen.height - sz.height) / 2;
        }
        return pt;
    }

    /**
     * Returns the dimension of the component applying the new width.
     * 
     * @param cmp
     *            The component.
     * @param width
     *            The desired width.
     * @return The new Dimension.
     */
    public static Dimension getWidthDimension(final JComponent cmp,
            final int width) {
        Dimension size = cmp.getPreferredSize();
        size.width = width;
        return size;
    }

    /**
     * Returns the dimension of the component applying the new height.
     * 
     * @param cmp
     *            The component.
     * @param height
     *            The desired height.
     * @return The new Dimension.
     */
    public static Dimension getHeightDimension(final JComponent cmp,
            final int height) {
        Dimension size = cmp.getPreferredSize();
        size.height = height;
        return size;
    }

    /**
     * Get the default insets.
     * 
     * @return The default insets.
     */
    public static Insets getDefaultInsets() {
        return defaultInsets;
    }

    /**
     * Set the default insets.
     * 
     * @param defaultInsets
     *            The default insets to set.
     */
    public static void setDefaultInsets(final Insets defaultInsets) {
        SwingUtil.defaultInsets = defaultInsets;
    }

    /**
     * Get the default horizontal gap.
     * 
     * @return The default horizontal gap.
     */
    public static int getDefaultHorizontalGap() {
        return defaultHorizontalGap;
    }

    /**
     * Set the default horizontal gap.
     * 
     * @param defaultHorizontalGap
     *            The default horizontal gap to set.
     */
    public static void setDefaultHorizontalGap(
            final int defaultHorizontalGap) {
        SwingUtil.defaultHorizontalGap = defaultHorizontalGap;
    }

    /**
     * Get the default vertical gap.
     * 
     * @return The default vertical gap.
     */
    public static int getDefaultVerticalGap() {
        return defaultVerticalGap;
    }

    /**
     * Set the default vertical gap.
     * 
     * @param defaultVerticalGap
     *            The default vertical gap to set.
     */
    public static void setDefaultVerticalGap(final int defaultVerticalGap) {
        SwingUtil.defaultVerticalGap = defaultVerticalGap;
    }

    /**
     * Returns the text for a key code.
     * 
     * @param keyCode
     *            The key code.
     * @return The key code text.
     */
    public static String getKeyCodeText(final int keyCode) {
        switch (keyCode) {
        case KeyEvent.VK_ENTER:
            return "Enter";
        case KeyEvent.VK_BACK_SPACE:
            return "Back";
        case KeyEvent.VK_TAB:
            return "Tab";
        case KeyEvent.VK_ESCAPE:
            return "Esc";
        case KeyEvent.VK_SPACE:
            return "Space";
        case KeyEvent.VK_PAGE_UP:
            return "Pg Up";
        case KeyEvent.VK_PAGE_DOWN:
            return "Pg Down";
        case KeyEvent.VK_END:
            return "End";
        case KeyEvent.VK_HOME:
            return "Home";
        case KeyEvent.VK_LEFT:
            return "Left";
        case KeyEvent.VK_UP:
            return "Up";
        case KeyEvent.VK_RIGHT:
            return "Right";
        case KeyEvent.VK_DOWN:
            return "Down";

            // modifiers
        case KeyEvent.VK_SHIFT:
            return "SHIFT";
        case KeyEvent.VK_CONTROL:
            return "CTRL";
        case KeyEvent.VK_ALT:
            return "ALT";
        case KeyEvent.VK_ALT_GRAPH:
            return "ALT Gr";

        case KeyEvent.VK_DELETE:
            return "Del";

        case KeyEvent.VK_F1:
            return "F1";
        case KeyEvent.VK_F2:
            return "F2";
        case KeyEvent.VK_F3:
            return "F3";
        case KeyEvent.VK_F4:
            return "F4";
        case KeyEvent.VK_F5:
            return "F5";
        case KeyEvent.VK_F6:
            return "F6";
        case KeyEvent.VK_F7:
            return "F7";
        case KeyEvent.VK_F8:
            return "F8";
        case KeyEvent.VK_F9:
            return "F9";
        case KeyEvent.VK_F10:
            return "F10";
        case KeyEvent.VK_F11:
            return "F11";
        case KeyEvent.VK_F12:
            return "F12";
        case KeyEvent.VK_F13:
            return "F13";
        case KeyEvent.VK_F14:
            return "F14";
        case KeyEvent.VK_F15:
            return "F15";
        case KeyEvent.VK_F16:
            return "F16";
        case KeyEvent.VK_F17:
            return "F17";
        case KeyEvent.VK_F18:
            return "F18";
        case KeyEvent.VK_F19:
            return "F19";
        case KeyEvent.VK_F20:
            return "F20";
        case KeyEvent.VK_F21:
            return "F21";
        case KeyEvent.VK_F22:
            return "F22";
        case KeyEvent.VK_F23:
            return "F23";
        case KeyEvent.VK_F24:
            return "F24";

        case KeyEvent.VK_INSERT:
            return "Insert";

        case KeyEvent.VK_KP_UP:
            return "Up";
        case KeyEvent.VK_KP_DOWN:
            return "Down";
        case KeyEvent.VK_KP_LEFT:
            return "Left";
        case KeyEvent.VK_KP_RIGHT:
            return "Right";
        }
        return "";
    }

    /**
     * Returns the value of a key code given its text.
     * 
     * @param text
     *            The key code text.
     * @return The key code value.
     */
    public static int getKeyCodeValue(final String text) {
        if (text.equalsIgnoreCase("Enter")) {
            return KeyEvent.VK_ENTER;
        }
        if (text.equalsIgnoreCase("Backspace")) {
            return KeyEvent.VK_BACK_SPACE;
        }
        if (text.equalsIgnoreCase("Tab")) {
            return KeyEvent.VK_TAB;
        }
        if (text.equalsIgnoreCase("Escape")) {
            return KeyEvent.VK_ESCAPE;
        }
        if (text.equalsIgnoreCase("Space")) {
            return KeyEvent.VK_SPACE;
        }
        if (text.equalsIgnoreCase("Page Up")) {
            return KeyEvent.VK_PAGE_UP;
        }
        if (text.equalsIgnoreCase("Page Down")) {
            return KeyEvent.VK_PAGE_DOWN;
        }
        if (text.equalsIgnoreCase("End")) {
            return KeyEvent.VK_END;
        }
        if (text.equalsIgnoreCase("Home")) {
            return KeyEvent.VK_HOME;
        }
        if (text.equalsIgnoreCase("Left")) {
            return KeyEvent.VK_LEFT;
        }
        if (text.equalsIgnoreCase("Up")) {
            return KeyEvent.VK_UP;
        }
        if (text.equalsIgnoreCase("Right")) {
            return KeyEvent.VK_RIGHT;
        }
        if (text.equalsIgnoreCase("Down")) {
            return KeyEvent.VK_DOWN;
        }

        // modifiers
        if (text.equalsIgnoreCase("Shift")) {
            return KeyEvent.VK_SHIFT;
        }
        if (text.equalsIgnoreCase("Control")) {
            return KeyEvent.VK_CONTROL;
        }
        if (text.equalsIgnoreCase("Alt")) {
            return KeyEvent.VK_ALT;
        }
        if (text.equalsIgnoreCase("Alt Graph")) {
            return KeyEvent.VK_ALT_GRAPH;
        }

        if (text.equalsIgnoreCase("Delete")) {
            return KeyEvent.VK_DELETE;
        }

        if (text.equalsIgnoreCase("F1")) {
            return KeyEvent.VK_F1;
        }
        if (text.equalsIgnoreCase("F2")) {
            return KeyEvent.VK_F2;
        }
        if (text.equalsIgnoreCase("F3")) {
            return KeyEvent.VK_F3;
        }
        if (text.equalsIgnoreCase("F4")) {
            return KeyEvent.VK_F4;
        }
        if (text.equalsIgnoreCase("F5")) {
            return KeyEvent.VK_F5;
        }
        if (text.equalsIgnoreCase("F6")) {
            return KeyEvent.VK_F6;
        }
        if (text.equalsIgnoreCase("F7")) {
            return KeyEvent.VK_F7;
        }
        if (text.equalsIgnoreCase("F8")) {
            return KeyEvent.VK_F8;
        }
        if (text.equalsIgnoreCase("F9")) {
            return KeyEvent.VK_F9;
        }
        if (text.equalsIgnoreCase("F10")) {
            return KeyEvent.VK_F10;
        }
        if (text.equalsIgnoreCase("F11")) {
            return KeyEvent.VK_F11;
        }
        if (text.equalsIgnoreCase("F12")) {
            return KeyEvent.VK_F12;
        }
        if (text.equalsIgnoreCase("F13")) {
            return KeyEvent.VK_F13;
        }
        if (text.equalsIgnoreCase("F14")) {
            return KeyEvent.VK_F14;
        }
        if (text.equalsIgnoreCase("F15")) {
            return KeyEvent.VK_F15;
        }
        if (text.equalsIgnoreCase("F16")) {
            return KeyEvent.VK_F16;
        }
        if (text.equalsIgnoreCase("F17")) {
            return KeyEvent.VK_F17;
        }
        if (text.equalsIgnoreCase("F18")) {
            return KeyEvent.VK_F18;
        }
        if (text.equalsIgnoreCase("F19")) {
            return KeyEvent.VK_F19;
        }
        if (text.equalsIgnoreCase("F20")) {
            return KeyEvent.VK_F20;
        }
        if (text.equalsIgnoreCase("F21")) {
            return KeyEvent.VK_F21;
        }
        if (text.equalsIgnoreCase("F22")) {
            return KeyEvent.VK_F22;
        }
        if (text.equalsIgnoreCase("F23")) {
            return KeyEvent.VK_F23;
        }
        if (text.equalsIgnoreCase("F24")) {
            return KeyEvent.VK_F24;
        }

        if (text.equalsIgnoreCase("Insert")) {
            return KeyEvent.VK_INSERT;
        }

        return 0;
    }

    /**
     * Returns the text of a combination of key modifiers.
     * 
     * @param modifiers
     *            The key modifiers.
     * @return The correspondant text.
     */
    public static String getKeyModifiersText(final int modifiers) {
        StringBuffer b = new StringBuffer();
        if ((modifiers & KeyEvent.CTRL_DOWN_MASK) != 0) {
            b.append("Ctrl");
            b.append("+");
        }
        if ((modifiers & KeyEvent.ALT_DOWN_MASK) != 0) {
            b.append("Alt");
            b.append("+");
        }
        if ((modifiers & KeyEvent.SHIFT_DOWN_MASK) != 0) {
            b.append("Shift");
            b.append("+");
        }
        if ((modifiers & KeyEvent.ALT_GRAPH_DOWN_MASK) != 0) {
            b.append("Alt Graph");
            b.append("+");
        }
        if (b.length() > 0) {
            b.setLength(b.length() - 1); // remove trailing '+'
        }
        return b.toString();
    }

    /**
     * Returns the value of the key modifiers given a text of the form
     * Ctrl+Alt+Shift
     * 
     * @param text
     *            A text like Ctrl+Alt+Shift
     * @return The value of the key modifiers
     */
    public static int getKeyModifiersValue(final String text) {
        String[] words = StringUtils.split(text, "+");
        int modifiers = 0;
        for (int i = 0; i < words.length; i++) {
            if (words[i].equalsIgnoreCase("Ctrl")) {
                modifiers |= KeyEvent.CTRL_DOWN_MASK;
                continue;
            }
            if (words[i].equalsIgnoreCase("Alt")) {
                modifiers |= KeyEvent.ALT_DOWN_MASK;
                continue;
            }
            if (words[i].equalsIgnoreCase("Shift")) {
                modifiers |= KeyEvent.SHIFT_DOWN_MASK;
                continue;
            }
            if (words[i].equalsIgnoreCase("Alt Graph")) {
                modifiers |= KeyEvent.ALT_GRAPH_DOWN_MASK;
                continue;
            }
        }
        return modifiers;
    }

    /**
     * Returns the key stroke text.
     * 
     * @param keyStroke
     *            The key stroke.
     * @return The text that represents the key stroke.
     */
    public static String getKeyStrokeText(final KeyStroke keyStroke) {
        StringBuffer b = new StringBuffer();
        b.append(getKeyModifiersText(keyStroke.getModifiers()));
        if (b.length() > 0) {
            b.append("+");
        }
        b.append(getKeyCodeText(keyStroke.getKeyCode()));
        return b.toString();
    }

    /**
     * Returns a key stroke give a text that represents it.
     * 
     * @param text
     *            The key stroke text.
     * @return The key stroke.
     */
    public static KeyStroke getKeyStrokeValue(final String text) {
        int index = text.lastIndexOf("+");
        String keyCode = "";
        String modifiers = "";
        if (index >= 0) {
            modifiers = text.substring(0, index);
            keyCode = text.substring(index + 1);
        } else {
            keyCode = text;
        }
        return KeyStroke.getKeyStroke(getKeyCodeValue(keyCode),
            getKeyModifiersValue(modifiers));
    }

    /**
     * Returns the text for a swing constant.
     * 
     * @param constant
     *            The swing constant.
     * @return The text for the constant.
     */
    public static String getSwingConstantText(final int constant) {
        if (constant == SwingConstants.CENTER) {
            return "Center";
        }
        if (constant == SwingConstants.TOP) {
            return "Top";
        }
        if (constant == SwingConstants.LEFT) {
            return "Left";
        }
        if (constant == SwingConstants.BOTTOM) {
            return "Bottom";
        }
        if (constant == SwingConstants.RIGHT) {
            return "Right";
        }
        if (constant == SwingConstants.NORTH) {
            return "North";
        }
        if (constant == SwingConstants.NORTH_EAST) {
            return "North East";
        }
        if (constant == SwingConstants.EAST) {
            return "East";
        }
        if (constant == SwingConstants.SOUTH_EAST) {
            return "South East";
        }
        if (constant == SwingConstants.SOUTH) {
            return "South";
        }
        if (constant == SwingConstants.SOUTH_WEST) {
            return "South West";
        }
        if (constant == SwingConstants.WEST) {
            return "West";
        }
        if (constant == SwingConstants.NORTH_WEST) {
            return "North West";
        }
        if (constant == SwingConstants.HORIZONTAL) {
            return "Horizontal";
        }
        if (constant == SwingConstants.VERTICAL) {
            return "Vertical";
        }
        if (constant == SwingConstants.LEADING) {
            return "Leading";
        }
        if (constant == SwingConstants.TRAILING) {
            return "Trailing";
        }
        if (constant == SwingConstants.NEXT) {
            return "Next";
        }
        if (constant == SwingConstants.PREVIOUS) {
            return "Previous";
        }
        return "";
    }

    /**
     * Returns the swing constant value given its text.
     * 
     * @param text
     *            The swing constant text.
     * @return The swing constant value.
     */
    public static int getSwingConstantValue(final String text) {
        if (text.equals("Center")) {
            return SwingConstants.CENTER;
        }
        if (text.equals("Top")) {
            return SwingConstants.TOP;
        }
        if (text.equals("Left")) {
            return SwingConstants.LEFT;
        }
        if (text.equals("Bottom")) {
            return SwingConstants.BOTTOM;
        }
        if (text.equals("Right")) {
            return SwingConstants.RIGHT;
        }
        if (text.equals("North")) {
            return SwingConstants.NORTH;
        }
        if (text.equals("North East")) {
            return SwingConstants.NORTH_EAST;
        }
        if (text.equals("East")) {
            return SwingConstants.EAST;
        }
        if (text.equals("South East")) {
            return SwingConstants.SOUTH_EAST;
        }
        if (text.equals("South")) {
            return SwingConstants.SOUTH;
        }
        if (text.equals("South West")) {
            return SwingConstants.SOUTH_WEST;
        }
        if (text.equals("West")) {
            return SwingConstants.WEST;
        }
        if (text.equals("North West")) {
            return SwingConstants.NORTH_WEST;
        }
        if (text.equals("Horizontal")) {
            return SwingConstants.HORIZONTAL;
        }
        if (text.equals("Vertical")) {
            return SwingConstants.VERTICAL;
        }
        if (text.equals("Leading")) {
            return SwingConstants.LEADING;
        }
        if (text.equals("Trailing")) {
            return SwingConstants.TRAILING;
        }
        if (text.equals("Next")) {
            return SwingConstants.NEXT;
        }
        if (text.equals("Previous")) {
            return SwingConstants.PREVIOUS;
        }
        return 0;
    }

    /**
     * Registers the list af actions to the applicable components contained in
     * the top component.
     * 
     * @param topComponent
     *            The top component.
     * @param actions
     *            The list of actions.
     */
    public static void registerActions(final Component topComponent,
            final Action[] actions) {
        Component[] components = SwingUtil.getAllComponents(topComponent);
        for (int i = 0; i < components.length; i++) {
            if (components[i] instanceof JComponent) {
                JComponent cmp = (JComponent) components[i];
                for (int j = 0; j < actions.length; j++) {
                    Action action = actions[j];
                    KeyStroke keyStroke =
                        (KeyStroke) action
                            .getValue(Action.ACCELERATOR_KEY);
                    if (keyStroke != null) {
                        cmp.getActionMap().put(keyStroke.toString(),
                            action);
                        // cmp.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(keyStroke,keyStroke.toString());
                        // cmp.getInputMap(JComponent.WHEN_FOCUSED).put(keyStroke,keyStroke.toString());
                        cmp.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
                            .put(keyStroke, keyStroke.toString());
                        // cmp.getInputMap().put(keyStroke,keyStroke.toString());
                    }
                }
            }
        }
    }

    /**
     * Fires a key event. <br>
     * Example:
     * 
     * <pre>
	 * fireKeyEvent(button, KeyEvent.VK_TAB, '\t', KeyEvent.SHIFT_MASK);
	 * </pre>
     * 
     * @param component
     *            The source component.
     * @param keyCode
     *            The key code.
     * @param charCode
     *            The char code.
     * @param modifiers
     *            The modifiers.
     * @see KeyEvent
     */
    public static void fireKeyEvent(final Component component,
            final int keyCode, final char charCode, final int modifiers) {
        KeyEvent event =
            new KeyEvent(component, KeyEvent.KEY_PRESSED,
                System.currentTimeMillis(), modifiers, keyCode, charCode,
                KeyEvent.KEY_LOCATION_STANDARD);
        component.dispatchEvent(event);
        event =
            new KeyEvent(component, KeyEvent.KEY_RELEASED,
                System.currentTimeMillis(), modifiers, keyCode, charCode,
                KeyEvent.KEY_LOCATION_STANDARD);
        component.dispatchEvent(event);
    }

    /**
     * Asks if a component has a key listener, an input method listener, and a
     * mouse listener or not.
     * 
     * @param component
     *            The component to be checked.
     * @param clazz
     *            The listener to be checked.
     * @return a boolean
     */
    private static boolean hasListener(final Component component,
            final Class<?> clazz) {
        KeyListener[] keylisteners = component.getKeyListeners();
        for (KeyListener listener : keylisteners) {
            if (clazz.isInstance(listener)) {
                return true;
            }
        }
        InputMethodListener[] inputMethodListeners =
            component.getInputMethodListeners();
        for (InputMethodListener listener : inputMethodListeners) {
            if (clazz.isInstance(listener)) {
                return true;
            }
        }
        MouseListener[] mouseListeners = component.getMouseListeners();
        for (MouseListener listener : mouseListeners) {
            if (clazz.isInstance(listener)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Adds a behaviour to a text component. When a component gets the focus
     * then all of text in the component will be selected.
     * 
     * @param button
     *            The component where the capability has to be added.
     */
    public static void addSelectAndNavigateCapability(
            final AbstractButton button) {
        if (hasListener(button, SelectAndNavigate.class)) {
            return;
        }
        button.addKeyListener(new SelectAndNavigate(button));
    }

    /**
     * Adds the escape capability to a window (closes it).
     * 
     * @param window
     *            The window to be added the capability.
     */
    public static void addEscapeCapability(final Window window) {
        if (hasListener(window, EscapeCapability.class)) {
            return;
        }
        window.addKeyListener(new EscapeCapability(window));
    }

    /**
     * Class to handle escape capability to windows.
     * 
     * @author Mario Cánovas
     * @version 1.0
     */
    private static class EscapeCapability implements KeyListener {
        private final Window window;

        EscapeCapability(final Window window) {
            this.window = window;
        }

        public void keyPressed(final KeyEvent e) {
            if (e.getModifiers() > 0) {
                return;
            }
            if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                window.dispose();
            }
        }

        public void keyReleased(final KeyEvent e) {
        }

        public void keyTyped(final KeyEvent e) {
        }
    }

    /**
     * Private class to handle select and navigate capability to the buttons.
     * 
     * @author Mario Cánovas
     * @version 1.0
     */
    private static class SelectAndNavigate implements KeyListener {
        private final AbstractButton button;

        SelectAndNavigate(final AbstractButton button) {
            this.button = button;
        }

        public void keyPressed(final KeyEvent e) {
            if (button.isFocusOwner()) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (e.getModifiers() == 0) {
                        button.doClick(150);
                    } else if (e.getModifiers() == KeyEvent.SHIFT_MASK) {
                        button.transferFocusBackward();
                    } else {
                        button.transferFocus();
                    }
                } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                    fireKeyEvent(button, KeyEvent.VK_TAB, '\t', 0);
                    button.transferFocus();
                } else if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    button.transferFocusBackward();
                }
            }
        }

        public void keyReleased(final KeyEvent e) {
        }

        public void keyTyped(final KeyEvent e) {
        }
    }
}
