package org.sk.maskedpicker;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.HashMap;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;

/**
 * A date selection panel.
 * 
 * @author Mario Cánovas
 */
public class JPanelCalendar extends JPanel {

    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;

    /** Date clicked property constant id. */
    public final static String DATE_CLICKED = "date_clicked";

    /** Change Month property constant id. */
    public final static String MONTH_CHANGED = "month_changed";

    /** Default values. */
    private static Border defaultBorder = null;

    private static Color defaultFgColor = null;

    private static Color defaultBgColor = null;

    private Font defaultFont = new JButton().getFont();

    /** Set default values. */
    static {
        JButton button = new JButton();
        defaultBorder = button.getBorder();
        defaultBgColor = button.getBackground();
        defaultFgColor = button.getForeground();
    }

    /**
     * Structure to configure any date.
     */
    class DateCfg {
        Color bgColor = null;

        boolean enabled = true;

        Color fgColor = null;

        String label = null;

        DateCfg(final Color bgColor, final Color fgColor,
                final boolean enabled, final String label) {
            this.bgColor = bgColor;
            this.fgColor = fgColor;
            this.enabled = enabled;
            this.label = label;
        }

        @Override
        public String toString() {
            String s =
                "bg = " + ((bgColor == null) ? "" : bgColor.toString());
            s += "|fg = " + ((fgColor == null) ? "" : fgColor.toString());
            s += "|en = " + (new Boolean(enabled).toString());
            s += "|lb = " + label;

            return s;
        }
    }

    /**
     * A JButton class that contains the number of the day of the month.
     */
    public static class JButton extends javax.swing.JButton {
        private static final long serialVersionUID = 1L;

        /** The day of the month. */
        private int day = -1;

        /** Default constructor. */
        public JButton() {
            super();
        }

        /**
         * Get the day.
         * 
         * @return the day
         */
        public int getDay() {
            return day;
        }

        /**
         * Set the day.
         * 
         * @param day
         *            The day to set.
         */
        public void setDay(final int day) {
            this.day = day;
        }
    }

    /**
     * ActionEvent handler.
     */
    class ButtonActionHandler implements ActionListener {
        public void actionPerformed(final ActionEvent e) {
            handleButtonAction(e);
        }
    }

    private final ButtonActionHandler actionHandler =
        new ButtonActionHandler();

    private class KeyHandler implements KeyListener {

        public void keyPressed(final KeyEvent e) {
            Object o = e.getSource();
            int selButton = -1;
            for (int i = 0; i < buttons.length; i++) {
                if (o.equals(buttons[i])) {
                    selButton = i;
                    break;
                }
            }
            if ((e.getKeyCode() == KeyEvent.VK_RIGHT)
                && (e.getModifiers() == 0)) {
                SwingUtil.fireKeyEvent((Component) e.getSource(),
                    KeyEvent.VK_TAB, '\t', 0);
            } else if ((e.getKeyCode() == KeyEvent.VK_LEFT)
                && (e.getModifiers() == 0)) {
                SwingUtil.fireKeyEvent((Component) e.getSource(),
                    KeyEvent.VK_TAB, '\t', KeyEvent.SHIFT_MASK);
            } else if ((e.getKeyCode() == KeyEvent.VK_UP)
                && (e.getModifiers() == 0)) {
                requestFocus(selButton, -7);
            } else if ((e.getKeyCode() == KeyEvent.VK_DOWN)
                && (e.getModifiers() == 0)) {
                requestFocus(selButton, +7);
            } else if ((e.getKeyCode() == KeyEvent.VK_ENTER)
                && (e.getModifiers() == 0)) {
                ActionEvent actionEvent =
                    new ActionEvent(e.getSource(), 0, "ENTER");
                handleButtonAction(actionEvent);
            }
        }

        public void keyReleased(final KeyEvent e) {
        }

        public void keyTyped(final KeyEvent e) {
        }

        private void requestFocus(final int buttonIndex, final int amount) {
            if (buttonIndex < 0) {
                for (JButton button : buttons) {
                    if (button.isEnabled()) {
                        button.requestFocusInWindow();
                        break;
                    }
                }
                return;
            }
            int newButton = buttonIndex + amount;
            if ((newButton >= 0) && (newButton < 42)) {
                JButton button = buttons[newButton];
                if (button.isEnabled()) {
                    button.requestFocusInWindow();
                }
            }

        }

    }

    private final KeyHandler keyHandler = new KeyHandler();

    /** Short week days text. */
    private String[] shortWeekDays = null;

    /** Long week days text. */
    private String[] longWeekDays = null;

    /** Decide Short/Long week Days */
    private boolean showShortWeekDay = true;

    /** Labels for every week day. */
    private final JLabel[] labelsWeekDays = new JLabel[7];

    /** Month label. */
    private JLabel labelMonth = null;

    /** Minimum valid date. */
    private Date minDate = null;

    /** Maximum valid date. */
    private Date maxDate = null;

    /** A map to configure dates. */
    private final HashMap<Date, DateCfg> dateCfgMap =
        new HashMap<Date, DateCfg>();

    /** Default week day configurations. */
    private final DateCfg[] dayOfWeekCfg = new DateCfg[] {null, null,
            null, null, null, null, null };

    /** The current date. */
    private Calendar currentDate = null;

    /** Button to decrease months. */
    private JButton buttonMonthDecrease = null;

    /** Button to increase months. */
    private JButton buttonMonthIncrease = null;

    /** Month panel. */
    private JPanel panelMonth = null;

    /** Panel for days. */
    private JPanel panelDays = null;

    /** An array of buttons for every day. */
    private JButton[] buttons = null;

    /**
     * Default constructor.
     */
    public JPanelCalendar() {
        super();
        setCurrentDate(new Date());
        setMinDate((Date) null);
        setMaxDate((Date) null);
        initializeMainPanel();
    }

    /**
     * Constructor selecting : the {@link Font} to be used.
     * 
     * @param font
     *            The {@link Font} to be used
     */
    public JPanelCalendar(final Font font) {
        super();
        setCurrentDate(new Date());
        setMinDate((Date) null);
        setMaxDate((Date) null);
        setDefaultFont(font);
        initializeMainPanel();
    }

    /**
     * Constructor selecting : Short/Long label for week day
     * 
     * @param showShortDayWeek
     *            A boolean to show short week day labels.
     */
    public JPanelCalendar(final boolean showShortDayWeek) {
        super();
        setCurrentDate(new Date());
        setMinDate((Date) null);
        setMaxDate((Date) null);
        setShowShortWeekDay(showShortDayWeek);
        initializeMainPanel();
    }

    /**
     * Constructor selecting : Short/Long label for week day
     * 
     * @param showShortDayWeek
     *            A boolean to show short week day labels.
     * @param font
     *            The text font.
     */
    public JPanelCalendar(final boolean showShortDayWeek, final Font font) {
        super();
        setCurrentDate(new Date());
        setMinDate((Date) null);
        setMaxDate((Date) null);
        setShowShortWeekDay(showShortDayWeek);
        setDefaultFont(font);
        initializeMainPanel();
    }

    /**
     * Constructor selecting : Short/Long label for week day and current
     * (initial) date
     * 
     * @param showShortDayWeek
     *            A boolean to show short week day labels.
     * @param current
     *            The date to be shown when displayed at first.
     */
    public JPanelCalendar(final boolean showShortDayWeek,
            final Date current) {
        super();
        setCurrentDate(current);
        setMinDate((Date) null);
        setMaxDate((Date) null);
        setShowShortWeekDay(showShortDayWeek);
        initializeMainPanel();

    }

    /**
     * Constructor assinging minimum and maximum date.
     * 
     * @param minDate
     *            Minimum date
     * @param maxDate
     *            Maximum date
     */
    public JPanelCalendar(final Date minDate, final Date maxDate) {
        super();
        setCurrentDate(new Date());
        setMinDate(minDate);
        setMaxDate(maxDate);
        initializeMainPanel();
    }

    /**
     * Constructor assinging minimum, maximum and current date.
     * 
     * @param minDate
     *            Minimum date
     * @param maxDate
     *            Maximum date
     * @param currentDate
     *            Current date
     */
    public JPanelCalendar(final Date minDate, final Date maxDate,
            final Date currentDate) {
        super();
        setCurrentDate(currentDate);
        setMinDate(minDate);
        setMaxDate(maxDate);
        initializeMainPanel();
    }

    /**
     * Handle button events.
     */
    private void handleButtonAction(final ActionEvent e) {
        if (e.getSource() == getButtonMonthIncrease()) {
            int year = getCurrentYear();
            int month = getCurrentMonth();
            int day = getCurrentDay();
            month++;
            if (month > 12) {
                year++;
                month = 1;
            }
            int lastDay = Calendar.getDaysOfMonth(year, month);
            if (day > lastDay) {
                day = lastDay;
            }
            Date oldDate = getCurrentDate();
            setCurrentDate(Calendar.createDate(year, month, day));
            firePropertyChange(MONTH_CHANGED, oldDate,
                currentDate.toDate());
            configure();
            return;
        }

        if (e.getSource() == getButtonMonthDecrease()) {
            int year = getCurrentYear();
            int month = getCurrentMonth();
            int day = getCurrentDay();
            month--;
            if (month < 1) {
                year--;
                month = 12;
            }
            int lastDay = Calendar.getDaysOfMonth(year, month);
            if (day > lastDay) {
                day = lastDay;
            }
            Date oldDate = getCurrentDate();
            setCurrentDate(Calendar.createDate(year, month, day));
            firePropertyChange(MONTH_CHANGED, oldDate,
                currentDate.toDate());
            configure();
            return;
        }

        for (int i = 0; i < buttons.length; i++) {
            if (e.getSource() == buttons[i]) {
                if (buttons[i].getDay() == 0 || !buttons[i].isEnabled()) {
                    return;
                }
                setCurrentDate(Calendar.createDate(getCurrentYear(),
                    getCurrentMonth(), buttons[i].getDay()));
                firePropertyChange(DATE_CLICKED, null,
                    currentDate.toDate());
                configure();
                return;
            }
        }
    }

    /**
     * Configure the calendar settings.
     */
    public void configure() {

        getLabelMonth().setText(
            Calendar.getLongMonth(true, getCurrentMonth()) + " "
                + getCurrentYear());
        for (int i = 0; i < 7; i++) {
            getLabelWeekDay(i).setFont(defaultFont);
        }
        for (int i = 0; i < buttons.length; i++) {
            buttons[i].setFont(defaultFont);
        }
        Calendar firstDayOfMonth =
            new Calendar(getCurrentYear(), getCurrentMonth(), 1);
        int firstDayOfWeek = firstDayOfMonth.getDayOfWeek() - 1;
        firstDayOfWeek -= getCalendar().getFirstDayOfWeek() - 1;
        if (firstDayOfWeek < 0) {
            firstDayOfWeek += 7;
        }
        int day = 0;
        for (int i = 0; i < firstDayOfWeek; i++) {
            buttons[i].setText("..");
            buttons[i].setEnabled(false);
            buttons[i].setDay(0);
            day++;
        }
        int lastDay =
            Calendar.getDaysOfMonth(getCurrentYear(), getCurrentMonth());
        int count = day;
        for (int i = day; i < lastDay + day; i++) {
            buttons[i].setDay(i - day + 1);
            Date date =
                Calendar.createDate(getCurrentYear(), getCurrentMonth(),
                    buttons[i].getDay());
            DateCfg cfg = dateCfgMap.get(date);
            String label = Convert.unfFromInt(buttons[i].getDay());
            if (cfg != null && cfg.label != null) {
                label += " " + cfg.label;
            }
            buttons[i].setText(label);
            if ((buttons[i].getDay()) == getCurrentDay()) {
                buttons[i].setBorder(BorderFactory.createLineBorder(
                    Color.darkGray, 3));
            } else {
                buttons[i].setBorder(defaultBorder);
            }
            buttons[i].setEnabled(true);
            Calendar today = new Calendar(date);
            int dayOfWeek = today.get(Calendar.DAY_OF_WEEK);
            if (minDate != null) {
                if (minDate != null && date.compareTo(minDate) < 0) {
                    buttons[i].setEnabled(false);
                }
            }
            if (maxDate != null) {
                if (maxDate != null && date.compareTo(maxDate) > 0) {
                    buttons[i].setEnabled(false);
                }
            }

            boolean configured = false;
            DateCfg dowCfg = dayOfWeekCfg[dayOfWeek - 1];
            if (dowCfg != null) {
                Color bgColor = dowCfg.bgColor;
                Color fgColor = dowCfg.fgColor;
                boolean enabled = dowCfg.enabled;
                configured = true;
                if (buttons[i].isEnabled()) {
                    if (!enabled) {
                        if (buttons[i].getDay() == getCurrentDay()) {
                            buttons[i].setBorder(BorderFactory
                                .createLineBorder(Color.darkGray, 3));
                        } else {
                            Border iborder =
                                BorderFactory.createEtchedBorder(
                                    new Color(200, 200, 200), new Color(
                                        200, 200, 200));
                            Border oborder =
                                BorderFactory.createEtchedBorder(
                                    new Color(200, 200, 200), new Color(
                                        153, 153, 153));
                            Border border =
                                BorderFactory.createCompoundBorder(
                                    oborder, iborder);
                            buttons[i].setBorder(border);
                        }
                    }
                }
                if (bgColor != null) {
                    buttons[i].setBackground(bgColor);
                } else {
                    buttons[i].setBackground(defaultBgColor);
                }
                if (fgColor != null) {
                    buttons[i].setForeground(fgColor);
                } else {
                    buttons[i].setForeground(defaultFgColor);
                }
            }
            if (cfg != null) {
                configured = true;
                if (buttons[i].isEnabled()) {
                    if (!cfg.enabled) {
                        if ((buttons[i].getDay()) == getCurrentDay()) {
                            buttons[i].setBorder(BorderFactory
                                .createLineBorder(Color.darkGray, 3));
                        } else {
                            Border iborder =
                                BorderFactory.createEtchedBorder(
                                    new Color(200, 200, 200), new Color(
                                        200, 200, 200));
                            Border oborder =
                                BorderFactory.createEtchedBorder(
                                    new Color(200, 200, 200), new Color(
                                        153, 153, 153));
                            Border border =
                                BorderFactory.createCompoundBorder(
                                    oborder, iborder);
                            buttons[i].setBorder(border);
                        }
                    }
                }
                if (cfg.bgColor != null) {
                    buttons[i].setBackground(cfg.bgColor);
                } else {
                    buttons[i].setBackground(defaultBgColor);
                }
                if (cfg.fgColor != null) {
                    buttons[i].setForeground(cfg.fgColor);
                } else {
                    buttons[i].setForeground(defaultFgColor);
                }
            }
            if (!configured) {
                buttons[i].setBackground(defaultBgColor);
                buttons[i].setForeground(defaultFgColor);
            }
            count++;
        }
        for (int i = count; i < buttons.length; i++) {
            buttons[i].setText("..");
            buttons[i].setBorder(defaultBorder);
            buttons[i].setBackground(defaultBgColor);
            buttons[i].setForeground(defaultFgColor);
            buttons[i].setEnabled(false);
            buttons[i].setDay(0);
        }
        getButtonMonthDecrease().setEnabled(true);
        getButtonMonthIncrease().setEnabled(true);
        if (getMaxDate() != null || getMinDate() != null) {
            if (maxDate != null) {
                lastDay =
                    Calendar.getDaysOfMonth(getCurrentYear(),
                        getCurrentMonth());
                Date lastDate =
                    Calendar.createDate(getCurrentYear(),
                        getCurrentMonth(), lastDay);
                if (lastDate.compareTo(getMaxDate()) >= 0) {
                    getButtonMonthIncrease().setEnabled(false);
                }
            }
            if (minDate != null) {
                Date firstDate =
                    Calendar.createDate(getCurrentYear(),
                        getCurrentMonth(), 1);
                if (firstDate.compareTo(getMinDate()) <= 0) {
                    getButtonMonthDecrease().setEnabled(false);
                }
            }
        }
    }

    /**
     * Inititalizes the main panel.
     */
    private void initializeMainPanel() {
        setName("JPanelCalendar");
        setLayout(new GridBagLayout());
        GridBagConstraints constraints = null;

        // Month panel
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.insets = new Insets(4, 4, 2, 4);
        constraints.weightx = 0.0;
        constraints.weighty = 0.0;
        constraints.gridx = 0;
        constraints.gridy = 0;
        add(getPanelMonth(), constraints);

        // Days panel
        constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.BOTH;
        constraints.anchor = GridBagConstraints.NORTH;
        constraints.weightx = 1.0;
        constraints.weighty = 1.0;
        constraints.insets = new Insets(2, 4, 4, 4);
        constraints.gridx = 0;
        constraints.gridy = 1;
        add(getPanelDays(), constraints);

        // Action Listener
        getButtonMonthDecrease().addActionListener(actionHandler);
        getButtonMonthIncrease().addActionListener(actionHandler);
        for (int i = 0; i < buttons.length; i++) {
            buttons[i].addActionListener(actionHandler);
        }
        // Key Listener
        getButtonMonthDecrease().addKeyListener(keyHandler);
        getButtonMonthIncrease().addKeyListener(keyHandler);
        for (int i = 0; i < buttons.length; i++) {
            buttons[i].addKeyListener(keyHandler);
        }

    }

    /**
     * Returns the button to decrease months.
     * 
     * @return The button.
     */
    private JButton getButtonMonthDecrease() {
        if (buttonMonthDecrease == null) {
            buttonMonthDecrease = new JButton();
            buttonMonthDecrease.setName("JButtonMonthDecrease");
            buttonMonthDecrease.setText("<<");
            buttonMonthDecrease.setFont(getDefaultFont());
            buttonMonthDecrease.setMargin(new Insets(2, 4, 2, 4));
        }
        return buttonMonthDecrease;
    }

    /**
     * Returns the button to increase months.
     * 
     * @return The button.
     */
    private JButton getButtonMonthIncrease() {
        if (buttonMonthIncrease == null) {
            buttonMonthIncrease = new JButton();
            buttonMonthIncrease.setName("JButtonMonthIncrease");
            buttonMonthIncrease.setText(">>");
            buttonMonthIncrease.setFont(getDefaultFont());
            buttonMonthIncrease.setMargin(new Insets(2, 4, 2, 4));
        }
        return buttonMonthIncrease;
    }

    /**
     * @return the current calendar.
     */
    public Calendar getCalendar() {
        return currentDate;
    }

    /**
     * Get the current date.
     * 
     * @return The current date.
     */
    public Date getCurrentDate() {
        return currentDate.toDate();
    }

    /**
     * Returns the current day of the month.
     * 
     * @return The current day of the month.
     */
    public int getCurrentDay() {
        return currentDate.getDay();
    }

    /**
     * Returns the current month.
     * 
     * @return The current month.
     */
    public int getCurrentMonth() {
        return currentDate.getMonth();
    }

    /**
     * Returns the current year.
     * 
     * @return The current year.
     */
    public int getCurrentYear() {
        return currentDate.getYear();
    }

    /**
     * Return the label.
     * 
     * @return The label.
     */
    private JLabel getLabelMonth() {
        if (labelMonth == null) {
            labelMonth = new JLabel();
            labelMonth.setName("JLabelMonth");
            labelMonth.setText("");
            labelMonth.setHorizontalTextPosition(SwingConstants.CENTER);
            labelMonth.setHorizontalAlignment(SwingConstants.CENTER);
            labelMonth.setFont(defaultFont);
        }
        return labelMonth;
    }

    /**
     * Returns the label for week day 0.
     * 
     * @param day
     *            The day of the week.
     * @return The label.
     */
    private JLabel getLabelWeekDay(final int day) {
        if (labelsWeekDays[day] == null) {
            labelsWeekDays[day] = new JLabel();
            labelsWeekDays[day].setName("JLabelWeekDay" + day);
            labelsWeekDays[day].setBorder(new EtchedBorder());
            if (isShowShortWeekDay()) {
                labelsWeekDays[day].setText(getShortWeekDays()[day + 1]);
            } else {
                labelsWeekDays[day].setText(getLongWeekDays()[day + 1]);
            }
            labelsWeekDays[day]
                .setHorizontalAlignment(SwingConstants.CENTER);
            labelsWeekDays[day].setFont(defaultFont);
        }
        return labelsWeekDays[day];
    }

    private JPanel getPanelDays() {
        if (panelDays == null) {

            panelDays = new JPanel(new GridBagLayout());
            panelDays.setName("JPanelDays");

            // Day labels
            FontMetrics metrics = getFontMetrics(defaultFont);
            int height = metrics.getHeight();
            int width = 0;
            for (int i = 0; i < 7; i++) {
                width =
                    Math.max(width,
                        metrics.stringWidth(getLabelWeekDay(i).getText()));
            }
            for (int i = 0; i < 7; i++) {
                GridBagConstraints constraints = new GridBagConstraints();
                constraints.fill = GridBagConstraints.BOTH;
                constraints.anchor = GridBagConstraints.NORTHWEST;
                constraints.insets = new Insets(4, 2, 4, 2);
                constraints.gridx = i;
                constraints.gridy = 0;
                getLabelWeekDay(i).setPreferredSize(
                    new Dimension(width + 10, height + 10));
                panelDays.add(getLabelWeekDay(i), constraints);
            }

            // 6 rows of 7 buttons -> 7 cols * 6 rows = 42 buttons
            buttons = new JButton[42];
            int index = 0;
            for (int row = 1; row < 7; row++) {
                for (int col = 0; col < 7; col++) {
                    // Define the button
                    JButton button = new JButton();
                    button.setName("JButtonDay_" + col + "_" + row);
                    button.setText("..");
                    button.setMargin(new Insets(2, 4, 2, 4));
                    button.setFont(defaultFont);
                    // Set the constraints
                    GridBagConstraints constraints =
                        new GridBagConstraints();
                    constraints.fill = GridBagConstraints.BOTH;
                    constraints.anchor = GridBagConstraints.NORTHWEST;
                    constraints.insets = new Insets(2, 2, 2, 2);
                    constraints.gridx = col;
                    constraints.gridy = row;
                    // Add the button to the panel
                    panelDays.add(button, constraints);
                    // Assign the button to the array of buttons
                    buttons[index] = button;
                    index++;
                }
            }
        }
        return panelDays;
    }

    /**
     * Returns the month panel.
     * 
     * @return The month panel.
     */
    private JPanel getPanelMonth() {
        if (panelMonth == null) {

            panelMonth = new JPanel(new GridBagLayout());
            panelMonth.setName("JPanelMonth");

            GridBagConstraints constraints = null;

            // Decrease button
            constraints = new GridBagConstraints();
            constraints.anchor = GridBagConstraints.WEST;
            constraints.insets = new Insets(1, 4, 1, 4);
            constraints.gridx = 0;
            constraints.gridy = 0;
            panelMonth.add(getButtonMonthDecrease(), constraints);

            // Label month
            constraints.fill = GridBagConstraints.HORIZONTAL;
            constraints.insets = new Insets(1, 4, 1, 4);
            constraints.weightx = 1.0;
            constraints.weighty = 1.0;
            constraints.gridx = 1;
            constraints.gridy = 0;
            panelMonth.add(getLabelMonth(), constraints);

            // Increase button
            constraints = new GridBagConstraints();
            constraints.anchor = GridBagConstraints.EAST;
            constraints.insets = new Insets(1, 4, 1, 4);
            constraints.gridx = 2;
            constraints.gridy = 0;
            panelMonth.add(getButtonMonthIncrease(), constraints);
        }
        return panelMonth;
    }

    /**
     * Returns the array of short week days.
     * 
     * @return The array of names.
     */
    private String[] getShortWeekDays() {
        if (shortWeekDays == null) {
            String[] days = Calendar.getShortDays(true);
            shortWeekDays = new String[days.length];
            int fdow = Calendar.getInstance().getFirstDayOfWeek();
            for (int i = 1; i < days.length; i++) {
                int n = i + fdow - 1;
                if (n > 7) {
                    n -= 7;
                }
                shortWeekDays[i] = days[n];
            }
        }
        return shortWeekDays;
    }

    /**
     * Returns the array of short week days.
     * 
     * @return The array of names.
     */
    private String[] getLongWeekDays() {
        if (longWeekDays == null) {
            String[] days = Calendar.getLongDays(true);
            longWeekDays = new String[days.length];
            int fdow = Calendar.getInstance().getFirstDayOfWeek();
            for (int i = 1; i < days.length; i++) {
                int n = i + fdow - 1;
                if (n > 7) {
                    n -= 7;
                }
                longWeekDays[i] = days[n];
            }
        }
        return longWeekDays;
    }

    /**
     * Get the maximum valid date.
     * 
     * @return The maximum valid date.
     */
    public Date getMaxDate() {
        return maxDate;
    }

    /**
     * Set the maximum valid date.
     * 
     * @param maxDate
     *            The maximum valid date to set.
     */
    public void setMaxDate(final Date maxDate) {
        this.maxDate = maxDate;
    }

    /**
     * Get the minimum valid date.
     * 
     * @return The minimum valid date.
     */
    public Date getMinDate() {
        return minDate;
    }

    /**
     * Set the minimum valid date.
     * 
     * @param minDate
     *            The minimum valid date to set.
     */
    public void setMinDate(final Date minDate) {
        this.minDate = minDate;
    }

    /**
     * Configure a date.
     * 
     * @param date
     *            The date to configure.
     * @param bgColor
     *            Background color.
     * @param fgColor
     *            Foreground color.
     * @param enabled
     *            A boolean to enable/disable the date.
     * @param label
     *            A label.
     */
    public void setDateCfg(final Date date, final Color bgColor,
            final Color fgColor, final boolean enabled, final String label) {
        dateCfgMap
            .put(date, new DateCfg(bgColor, fgColor, enabled, label));
    }

    /**
     * Clear all dateCfg
     */
    public void clearDateCfg() {
        dateCfgMap.clear();
    }

    /**
     * Sets the default configuration for a day of the week.
     * 
     * @param day
     *            The day of the week.
     * @param bgColor
     *            Background color.
     * @param fgColor
     *            Foreground color.
     * @param enabled
     *            A boolean to enable/disable the date.
     */
    public void setDayOfWeekCfg(final int day, final Color bgColor,
            final Color fgColor, final boolean enabled) {
        if (day < 1 || day > 7) {
            throw new IllegalArgumentException("Invalid day");
        }
        dayOfWeekCfg[day - 1] =
            new DateCfg(bgColor, fgColor, enabled, null);
    }

    /**
     * Set the current date.
     * 
     * @param date
     *            The current date.
     */
    public void setCurrentDate(final Date date) {
        currentDate = new Calendar(date == null ? new Date() : date);
    }

    /**
     * @return the showShortWeekDay
     */
    protected boolean isShowShortWeekDay() {
        return showShortWeekDay;
    }

    /**
     * @param showShortWeekDay
     *            the showShortWeekDay to set
     */
    public void setShowShortWeekDay(final boolean showShortWeekDay) {
        this.showShortWeekDay = showShortWeekDay;
    }

    /**
     * Gets the default font.
     * 
     * @return the font.
     */
    public Font getDefaultFont() {
        return defaultFont;
    }

    /**
     * Sets the default font.
     * 
     * @param defaultFont
     *            the font to set.
     */
    public void setDefaultFont(final Font defaultFont) {
        this.defaultFont = defaultFont;
    }
}
