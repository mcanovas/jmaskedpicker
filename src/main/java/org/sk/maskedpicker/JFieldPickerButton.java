package org.sk.maskedpicker;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JPanel;

/**
 * Combines a <code>JMaskedField</code> and a <code>JButton</code> on a
 * <code>JPanel</code> to produce a component that's an entry field with an
 * associated action button.
 * 
 * @author Mario Cánovas
 */
public class JFieldPickerButton extends JPanel {

    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default text field height.
     */
    protected final int DEFAULT_TEXTFIELD_HEIGHT = 18;

    /**
     * A flag that indicates if the full component is enabled or not.
     */
    private boolean enabled = true;

    /**
     * A flag that indicates if the text field is enabled or not. When the full
     * component is disabled the text field will be disabled too.
     */
    private boolean textEnabled = true;

    /**
     * The key listener that fires key down on the edit control and the button.
     */
    private class KeyDown implements KeyListener {
        public void keyPressed(final KeyEvent e) {
        }

        public void keyReleased(final KeyEvent e) {
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                getButton().doClick();
            }
        }

        public void keyTyped(final KeyEvent e) {
        }
    }

    /**
     * The action listener that will listen the button to fire the lookup
     * action.
     */
    private class LookupActionListener implements ActionListener {
        public void actionPerformed(final ActionEvent e) {
            if (getContext().getActionLookup() != null) {
                getContext().getActionLookup().actionPerformed(e);
            }
        }
    }

    /** Internal text field. */
    private JFieldPicker textField = null;

    /** Internal button. */
    private JButton button = null;

    private final Icon _defaultIcon;

    /**
     * Default constructor.
     */
    public JFieldPickerButton() {
        this(new IconArrow());
    }

    /**
     * Constructor with icon as a parameter.
     * 
     * @param icon
     *            the icon.
     */
    public JFieldPickerButton(final Icon icon) {
        super();
        _defaultIcon = icon;
        initialize();
    }

    private void initialize() {
        setOpaque(false);
        setSize(273, DEFAULT_TEXTFIELD_HEIGHT);
        setLayout(new GridBagLayout());

        GridBagConstraints constraintsField = new GridBagConstraints();
        constraintsField.gridx = 0;
        constraintsField.gridy = 0;
        constraintsField.fill = GridBagConstraints.HORIZONTAL;
        constraintsField.weightx = 1.0;
        add(getTextField(), constraintsField);

        GridBagConstraints constraintsButton = new GridBagConstraints();
        constraintsButton.gridx = 1;
        constraintsButton.gridy = 0;
        add(getButton(), constraintsButton);

        KeyDown keyDown = new KeyDown();
        getTextField().addKeyListener(keyDown);
    }

    /**
     * Adds a key listener to the text field.
     * 
     * @param l
     *            The listener.
     */
    @Override
    public void addKeyListener(final KeyListener l) {
        getTextField().addKeyListener(l);
        getButton().addKeyListener(l);
    }

    /**
     * Returns the text field.
     * 
     * @return The text field component.
     */
    public JFieldPicker getTextField() {
        if (textField == null) {
            textField = new JFieldPicker();
        }
        return textField;
    }

    /**
     * Returns the action button.
     * 
     * @return The button.
     */
    public JButton getButton() {
        if (button == null) {
            Dimension sz =
                new Dimension(DEFAULT_TEXTFIELD_HEIGHT,
                    DEFAULT_TEXTFIELD_HEIGHT);
            button = new JButton();
            button.setIcon(_defaultIcon);
            button.setMinimumSize(sz);
            button.setMaximumSize(sz);
            button.setPreferredSize(sz);
            button.setMargin(new Insets(0, 0, 0, 0));
            button.addActionListener(new LookupActionListener());
            button.setFocusable(false);
        }
        return button;
    }

    /**
     * Sets the name of this panel, the text field and the button.
     */
    @Override
    public void setName(final String name) {
        super.setName(name);
        getTextField().setName(name + "_TextField");
        getButton().setName(name + "_Button");
    }

    /**
     * Check if the field is editable.
     * 
     * @return A <code>boolean</code>.
     */
    public boolean isEditable() {
        return getTextField().isEditable();
    }

    /**
     * Check if the field is enabled.
     * 
     * @return A <code>boolean</code>.
     */
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    /**
     * Editable/not editable.
     * 
     * @param editable
     *            A <code>boolean</code>
     */
    public void setEditable(final boolean editable) {
        getTextField().setEditable(editable);
        getButton().setVisible(editable);
    }

    /**
     * Enable/disable.
     * 
     * @param enabled
     *            A <code>boolean</code>
     */
    @Override
    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
        if (enabled) {
            getTextField().setEnabled(textEnabled);
        } else {
            getTextField().setEnabled(false);
        }
        getButton().setEnabled(enabled);
    }

    /**
     * Enables / disables the text field only.
     * 
     * @param enabled
     *            a <code>boolean</code>
     */
    public void setTextEnabled(final boolean enabled) {
        textEnabled = enabled;
        if (isEnabled()) {
            getTextField().setEnabled(enabled);
        }
    }

    /**
     * A flag that indicates if the text field is enabled or not.
     * 
     * @return a boolean.
     */
    public boolean isTextEnabled() {
        return textEnabled;
    }

    /*
     * (non-Javadoc)
     * @see xvr.com.lib.swing.EntityField#clear()
     */
    public void clear() {
        getTextField().clear();
    }

    /*
     * (non-Javadoc)
     * @see xvr.com.lib.swing.EntityField#setValue(xvr.com.lib.entity.Value)
     */
    public void setValue(final String value) {
        getTextField().setValue(value);
    }

    /*
     * (non-Javadoc)
     * @see xvr.com.lib.swing.EntityLabel#getContext()
     */
    public FieldPikerContext getContext() {
        return getTextField().getContext();
    }

    /*
     * (non-Javadoc)
     * @see
     * xvr.com.lib.swing.EntityLabel#setContext(xvr.com.lib.swing.ComponentContext
     * )
     */
    public void setContext(final FieldPikerContext context) {
        getTextField().setContext(context);
    }

    /*
     * (non-Javadoc)
     * @see xvr.com.lib.swing.EntityField#getValue()
     */
    public String getValue() {
        return getTextField().getValue();
    }

    /*
     * (non-Javadoc)
     * @see xvr.com.lib.swing.EntityField#getComponent()
     */
    public Component getComponent() {
        return textField.getComponent();
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.JComponent#requestFocus()
     */
    @Override
    public void requestFocus() {
        getComponent().requestFocus();
    }

    /*
     * (non-Javadoc)
     * @see java.awt.Component#isFocusOwner()
     */
    @Override
    public boolean isFocusOwner() {
        return getComponent().isFocusOwner();
    }

}
