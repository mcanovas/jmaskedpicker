package org.sk.maskedpicker;

import java.sql.Date;
import java.text.ParseException;
import java.util.Locale;

/**
 * A date formatter that acts as expected.
 * 
 * @author Mario Cánovas
 */
public class DateFormatter extends MaskFormatter {

    /** serial version UID. */
    private static final long serialVersionUID = 1L;

    /** The locale. */
    private Locale locale = null;

    /**
     * Default constructor using the default locale.
     */
    public DateFormatter() {
        this(Locale.getDefault());
    }

    /**
     * Constructor assigning the locale.
     * 
     * @param locale
     *            The locale to use.
     */
    public DateFormatter(final Locale locale) {
        super();
        this.locale = locale;
        setMask(getDateMask(Convert.getNormalizedDatePattern(locale)));
    }

    /**
     * Convert the string to a value.
     */
    @Override
    public Object stringToValue(final String value) throws ParseException {
        return Convert.fmtToDate(value, locale);
    }

    /**
     * Convert the value to a string.
     */
    @Override
    public String valueToString(final Object value) throws ParseException {
        String str = Convert.fmtFromDate((Date) value, locale);
        if (str.trim().length() == 0) {
            return super.valueToString(value);
        }
        return str;
    }

    /**
     * Returns the mask that this mask formatter must use.
     * 
     * @return The mask.
     */
    private String getDateMask(final String datePattern) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < datePattern.length(); i++) {
            char c = datePattern.charAt(i);
            switch (c) {
            case 'd':
            case 'M':
            case 'y':
            case 'H':
            case 'm':
            case 's':
                buffer.append("#");
                break;
            default:
                buffer.append(c);
                break;
            }
        }
        return buffer.toString();
    }
}
