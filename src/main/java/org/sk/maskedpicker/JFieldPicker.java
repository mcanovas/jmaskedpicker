package org.sk.maskedpicker;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyListener;

import javax.swing.JFormattedTextField;
import javax.swing.JPanel;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.PlainDocument;

/**
 * A formatted text field that implements EntityField.
 * 
 * @author Mario Cánovas
 */
public class JFieldPicker extends JPanel {

    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Default text field height.
     */
    protected final int DEFAULT_TEXTFIELD_HEIGHT = 18;

    /** Component context. */
    private FieldPikerContext _context = null;

    /** The underlying text field. */
    private JFormattedTextField _textField = null;

    /**
     * The initial size of this component.
     */
    private Dimension initialSize = null;

    /** Plain string document to handle field properties. */
    public class StringDocument extends PlainDocument {
        private static final long serialVersionUID = 1L;

        private boolean uppercase = false;

        private int maxLength = 0;

        /**
         * Constructor.
         * 
         * @param maxLength
         *            The maximum length.
         * @param uppercase
         *            A flag to indicate that content is uppercase.
         */
        public StringDocument(final int maxLength, final boolean uppercase) {
            super();
            this.maxLength = maxLength;
            this.uppercase = uppercase;
        }

        /*
         * (non-Javadoc)
         * @see javax.swing.text.Document#insertString(int, java.lang.String,
         * javax.swing.text.AttributeSet)
         */
        @Override
        public void insertString(final int offs, String str,
                final AttributeSet a) throws BadLocationException {
            if (uppercase) {
                str = str.toUpperCase();
            }
            String text = getText(0, getLength());
            if (text.length() > 0
                && text.length() + str.length() > maxLength) {
                if (offs > maxLength) {
                    return;
                }
                super.remove(offs, str.length());
            }
            super.insertString(offs, str, a);
        }
    }

    /**
     * Default constructor.
     */
    public JFieldPicker() {
        super();
        setOpaque(false);
        setSize(273, DEFAULT_TEXTFIELD_HEIGHT);
        setLayout(new GridBagLayout());

        GridBagConstraints constraintsField = new GridBagConstraints();
        constraintsField.gridx = 0;
        constraintsField.gridy = 0;
        constraintsField.fill = GridBagConstraints.HORIZONTAL;
        constraintsField.weightx = 1.0;
        add(getTextField(), constraintsField);
    }

    /*
     * (non-Javadoc)
     * @see java.awt.Component#addKeyListener(java.awt.event.KeyListener)
     */
    @Override
    public synchronized void addKeyListener(final KeyListener l) {
        getTextField().addKeyListener(l);
    }

    /**
     * Get the component context.
     * 
     * @return The component context.
     */
    public FieldPikerContext getContext() {
        return _context;
    }

    /**
     * Set this component context and apply the field label.
     * 
     * @param context
     *            The component context.
     */
    public void setContext(final FieldPikerContext context) {
        _context = context;
        configure();
    }

    /**
     * Clear the control with its default data.
     */
    public void clear() {
        setValue(getContext().getDefaultValue());
    }

    /**
     * Update the value of the component.
     * 
     * @param value
     *            The value to set.
     */
    public void setValue(final String value) {
        _textField.setText(value);
    }

    public String getValue() {
        return _textField.getText();
    }

    /*
     * (non-Javadoc)
     * @see xvr.com.lib.swing.EntityLabel#isEnabled()
     */
    @Override
    public boolean isEnabled() {
        if (_textField != null) {
            return _textField.isEnabled();
        }
        return false;
    }

    /*
     * (non-Javadoc)
     * @see xvr.com.lib.swing.EntityLabel#setEnabled(boolean)
     */
    @Override
    public void setEnabled(final boolean enabled) {
        if (_textField != null) {
            _textField.setEnabled(enabled);
        }
    }

    /**
     * Returns the text field.
     * 
     * @return The text field component.
     */
    public JFormattedTextField getTextField() {
        if (_textField == null) {
            _textField = new JFormattedTextField();
        }
        return _textField;
    }

    /**
     * Check if the text field is editable.
     * 
     * @return A boolean
     */
    public boolean isEditable() {
        return getTextField().isEditable();
    }

    /**
     * Set the text field editable flag.
     * 
     * @param editable
     *            The text field editable flag.
     */
    public void setEditable(final boolean editable) {
        getTextField().setEditable(editable);
    }

    /**
     * Redefines this component.
     */
    public void redefineComponent() {
        // Resize.
        setMinimumSize(initialSize);
        setPreferredSize(initialSize);
    }

    /**
     * Sets the initial size.
     * 
     * @param initialSize
     *            The initial size to be set.
     */
    public void setInitialSize(final Dimension initialSize) {
        this.initialSize = initialSize;
    }

    /**
     * Gets the initial size.
     * 
     * @return the initial size.
     */
    public Dimension getInitialSize() {
        return initialSize;
    }

    /*
     * (non-Javadoc)
     * @see xvr.com.lib.swing.EntityField#getComponent()
     */
    public Component getComponent() {
        return _textField;
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.JComponent#requestFocus()
     */
    @Override
    public void requestFocus() {
        getComponent().requestFocus();
    }

    /*
     * (non-Javadoc)
     * @see java.awt.Component#isFocusOwner()
     */
    @Override
    public boolean isFocusOwner() {
        return getComponent().isFocusOwner();
    }

    protected void configure() {
        final JFormattedTextField jTextField = getTextField();

        // Allow all text entered.
        jTextField.setFocusLostBehavior(JFormattedTextField.COMMIT);

        // Generic behaviour when value changes.
        jTextField.addPropertyChangeListener("value",
            new java.beans.PropertyChangeListener() {
                public void propertyChange(
                        final java.beans.PropertyChangeEvent e) {
                    // System.out.println("propertyChange(value)");
                }
            });

        // Generic behaviour for alignment.
        jTextField
            .setHorizontalAlignment(_context.getHorizontalAligment());

        jTextField.setFormatterFactory(new DefaultFormatterFactory(
            new DateFormatter(_context.getLocale())));

    }
}
