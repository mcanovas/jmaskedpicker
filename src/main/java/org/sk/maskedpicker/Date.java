package org.sk.maskedpicker;

/**
 * A date that masks out the time information. An SQL date should not have time
 * info.
 * 
 * @author Mario Cánovas
 */
public class Date extends java.sql.Date {
    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Number of millis in a day
     */
    public static final long day = 24 * 60 * 60 * 1000;

    /**
     * Default constructor..
     */
    public Date() {
        this(System.currentTimeMillis());
    }

    /**
     * Creates a new instance of Date.
     * 
     * @param time
     *            milliseconds since January 1, 1970, 00:00:00 GMT not to exceed
     *            the milliseconds representation for the year 8099. A negative
     *            number indicates the number of milliseconds before January 1,
     *            1970, 00:00:00 GMT.
     */
    public Date(final long time) {
        super(time);
    }

    /**
     * Copy constructor.
     * 
     * @param date
     *            The date to be copied.
     */
    public Date(final java.sql.Date date) {
        this(date.getTime());
    }

    /*
     * (non-Javadoc)
     * @see java.util.Date#compareTo(java.util.Date)
     */
    @Override
    public int compareTo(final java.util.Date anotherDate) {
        Calendar thisCalendar = new Calendar(this);
        Calendar anotherCalendar = new Calendar(anotherDate.getTime());
        long yeardif = thisCalendar.getYear() - anotherCalendar.getYear();
        if (yeardif > 0) {
            return 1;
        }
        if (yeardif < 0) {
            return -1;
        }
        long monthdif =
            thisCalendar.getMonth() - anotherCalendar.getMonth();
        if (monthdif > 0) {
            return 1;
        }
        if (monthdif < 0) {
            return -1;
        }
        long daydif = thisCalendar.getDay() - anotherCalendar.getDay();
        if (daydif > 0) {
            return 1;
        }
        if (daydif < 0) {
            return -1;
        }
        return 0;
    }

    /*
     * (non-Javadoc)
     * @see java.util.Date#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        return (compareTo((Date) obj) == 0);
    }

    /*
     * (non-Javadoc)
     * @see java.util.Date#after(java.util.Date)
     */
    @Override
    public boolean after(final java.util.Date when) {
        return (compareTo(when) > 0);
    }

    /*
     * (non-Javadoc)
     * @see java.util.Date#before(java.util.Date)
     */
    @Override
    public boolean before(final java.util.Date when) {
        return (compareTo(when) < 0);
    }

    private static void test(final Date d1, final Date d2) {
        System.out.println("D1: " + d1 + " T:" + d1.getTime());
        System.out.println("D2: " + d2 + " T:" + d2.getTime());
        System.out.println("Compare: " + d1.compareTo(d2));
        System.out.println("Equals: " + d1.equals(d2));
        System.out.println("After: " + d1.after(d2));
        System.out.println("Before: " + d1.before(d2));
    }

    /**
     * Test method.
     * 
     * @param args
     *            Not used.
     */
    public static void main(final String[] args) {
        Date d1 = new Calendar(2008, 5, 12).toDate();
        try {
            Thread.sleep(300);
        } catch (InterruptedException e) {
        }
        Date d2 = new Date(new java.sql.Date(System.currentTimeMillis()));
        test(d1, d2);
    }
}
