package org.sk.maskedpicker;

import java.util.Date;

/**
 * A time that masks out date and millisecond information, referring always to
 * the same 0 day. Note that an SQL time does not have millisecond info. Use a
 * timestamp if millis are needed.
 * 
 * @author Mario Cánovas
 */
public class Time extends java.sql.Time {
    private static final long serialVersionUID = 1L;

    /**
     * Number of milliseconds a day.
     */
    public static final long day = 24 * 60 * 60 * 1000;

    /**
     * Default constructor.
     */
    public Time() {
        this(System.currentTimeMillis());
    }

    /**
     * Creates a new instance of Time.
     * 
     * @param time
     *            The time in milliseconds.
     */
    public Time(final long time) {
        super(time);
    }

    /**
     * Copy constructor.
     * 
     * @param time
     *            The time to be copied.
     */
    public Time(final java.sql.Time time) {
        this(time.getTime());
    }

    /*
     * (non-Javadoc)
     * @see java.util.Date#equals(java.lang.Object)
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        return (compareTo((Date) obj) == 0);
    }

    /*
     * (non-Javadoc)
     * @see java.util.Date#after(java.util.Date)
     */
    @Override
    public boolean after(final java.util.Date when) {
        return (compareTo(when) > 0);
    }

    /*
     * (non-Javadoc)
     * @see java.util.Date#before(java.util.Date)
     */
    @Override
    public boolean before(final java.util.Date when) {
        return (compareTo(when) < 0);
    }

    /*
     * (non-Javadoc)
     * @see java.util.Date#compareTo(java.util.Date)
     */
    @Override
    public int compareTo(final Date anotherDate) {
        Calendar thisCalendar = new Calendar(this);
        Calendar anotherCalendar = new Calendar(anotherDate.getTime());
        long hourdif = thisCalendar.getHour() - anotherCalendar.getHour();
        if (hourdif > 0) {
            return 1;
        }
        if (hourdif < 0) {
            return -1;
        }
        long mindif =
            thisCalendar.getMinute() - anotherCalendar.getMinute();
        if (mindif > 0) {
            return 1;
        }
        if (mindif < 0) {
            return -1;
        }
        long secdif =
            thisCalendar.getSecond() - anotherCalendar.getSecond();
        if (secdif > 0) {
            return 1;
        }
        if (secdif < 0) {
            return -1;
        }
        long milidif =
            thisCalendar.getMilliSecond()
                - anotherCalendar.getMilliSecond();
        if (milidif > 0) {
            return 1;
        }
        if (milidif < 0) {
            return -1;
        }
        return 0;
    }

}
