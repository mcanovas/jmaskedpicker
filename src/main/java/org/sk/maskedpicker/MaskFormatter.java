package org.sk.maskedpicker;

import java.text.ParseException;

/**
 * An extension of javax.swing.text.MaskFormatter to avoid parse exceptions.
 * 
 * @author Mario Cánovas
 */
public class MaskFormatter extends javax.swing.text.MaskFormatter {

    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Cefault constructor.
     */
    public MaskFormatter() {
        super();
    }

    /**
     * @param mask
     */
    public MaskFormatter(final String mask) {
        super();
        setMask(mask);
    }

    /*
     * (non-Javadoc)
     * @see javax.swing.text.MaskFormatter#setMask(java.lang.String)
     */
    @Override
    public void setMask(final String mask) {
        try {
            super.setMask(mask);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
