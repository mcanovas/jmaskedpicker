package org.sk.maskedpicker;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Main {

    /**
     * @param args
     */
    public static void main(final String[] args) {
        JFrame frame = new JFrame();
        frame.setExtendedState(JFrame.MAXIMIZED_BOTH);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        Icon icon =
            new ImageIcon(
                Main.class.getResource("/img/Calendar_1_Day.png"));
        JFieldPickerDate date = new JFieldPickerDate(icon);
        date.setEnabled(false);
        frame.add(date);
        frame.setVisible(true);
    }
}
