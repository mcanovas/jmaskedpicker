package org.sk.maskedpicker;

import java.math.BigDecimal;

/**
 * Number utilities extended from Apache Commons Lang.
 * 
 * @author Mario Cánovas
 */
public class NumberUtils extends org.apache.commons.lang.math.NumberUtils {
    /**
     * Round a number.
     * <p>
     * 
     * @param value
     *            The value to round.
     * @param decimals
     *            The number of decimal places.
     * @return The rounded value.
     */
    public static double round(final double value, final int decimals) {
        double p = java.lang.Math.pow(10, decimals);
        double v = value * p;
        long l = java.lang.Math.round(v);
        double r = l / p;
        return r;
    }

    /**
     * Gets the default number as a string using the desired decimals.
     * 
     * @param decimals
     *            The desired decimals
     * @return a string.
     */
    public static String defaultStringNumber(final int decimals) {
        StringBuffer b = new StringBuffer();
        b.append("0");
        if (decimals > 0) {
            b.append(".");
            b.append(StringUtils.repeat("0", decimals));
        }
        return b.toString();
    }

    /**
     * Returns a big decimal.
     * 
     * @return The <code>BigDecimal</code>.
     * @param str
     *            The text string.
     * @param dec
     *            Tne decimal places.
     * @param decPoint
     *            A flag telling if the decimal point is included.
     */
    public static BigDecimal getBigDecimal(final String str,
            final int dec, final boolean decPoint) {
        if (dec == 0) {
            return new BigDecimal(str);
        }
        int len = str.length();
        if (decPoint) {
            return new BigDecimal(str).setScale(dec,
                BigDecimal.ROUND_HALF_UP);
        } else {
            return new BigDecimal(str.substring(0, len - dec) + "."
                + str.substring(len - dec)).setScale(dec,
                BigDecimal.ROUND_HALF_UP);
        }
    }

    /**
     * Verifies and corrects numeric format.
     * 
     * @return The result string.
     * @param str
     *            The source string.
     */
    public static String numberFormat(String str) {
        // Strip spaces at both ends.
        str = str.trim();
        // Sign can be on the left and on the right. if its on the left, there
        // can be
        // spaces between the sign and the number itself.
        int len = str.length();
        boolean sign =
            str.charAt(0) == '-' || str.charAt(0) == '+'
                || str.charAt(len - 1) == '-'
                || str.charAt(len - 1) == '+';
        // If there is a sign, put it in the right place.
        if (sign) {
            if (str.charAt(0) == '-' || str.charAt(0) == '+') {
                if (str.charAt(1) == ' ') {
                    str = str.substring(0, 1) + str.substring(1).trim();
                }
            } else {
                str =
                    str.substring(len - 1, len)
                        + str.substring(0, len - 1);
            }
            // Strip positive sign.
            if (str.charAt(0) == '+') {
                str = str.substring(1);
            }
        }
        return str;
    }
}
