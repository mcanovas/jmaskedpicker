package org.sk.maskedpicker;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Locale;

/**
 * Utility class to convert from/to a <code>String</code> to/from the supported
 * JDBC types.
 * 
 * @author Mario Cánovas
 */
public final class Convert {

    /** Special save chars for files encoded like property files. */
    private static final String specialSaveChars = "=: \t\r\n\f#!";

    /** A table of hex digits */
    private static final char[] hexDigit = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

    /** Container for date format map items. */
    private static class FormatItem {
        SimpleDateFormat format = null;

        String pattern = null;
    }

    /** A localized date format map. */
    private static HashMap<Locale, FormatItem> dateFormatMap = null;

    /** A localized time format map. */
    private static HashMap<Locale, FormatItem> timeFormatMap = null;

    /** A localized timestamp format map. */
    private static HashMap<Locale, FormatItem> timestampFormatMap = null;

    /**
     * Convert from a <code>BigDecimal</code> forcing the scale.
     * 
     * @return A string.
     * @param number
     *            A number as a <code>java.math.BigDecimal</code>
     * @param scale
     *            The scale.
     * @param locale
     *            The desired locale.
     */
    public static String fmtFromBigDecimal(final BigDecimal number,
            final int scale, final Locale locale) {
        if (number == null) {
            return "";
        }
        NumberFormat formatted = NumberFormat.getNumberInstance(locale);
        formatted.setMaximumFractionDigits(scale);
        formatted.setMinimumFractionDigits(scale);
        return formatted.format(number.doubleValue());
    }

    /**
     * Convert from a <code>BigDecimal</code> forcing the scale.
     * 
     * @return A string.
     * @param number
     *            A number as a <code>java.math.BigDecimal</code>
     * @param scale
     *            The scale.
     */
    public static String fmtFromBigDecimal(final BigDecimal number,
            final int scale) {
        return fmtFromBigDecimal(number, scale, Locale.getDefault());
    }

    /**
     * Convert from a <code>BigDecimal</code>.
     * 
     * @return A string.
     * @param number
     *            The number to conver as a <code>BigDecimal</code>
     * @param locale
     *            The appliable locale.
     */
    public static String fmtFromBigDecimal(final BigDecimal number,
            final Locale locale) {
        if (number == null) {
            return "";
        }
        NumberFormat formatted = NumberFormat.getNumberInstance(locale);
        formatted.setMaximumFractionDigits(number.scale());
        formatted.setMinimumFractionDigits(number.scale());
        return formatted.format(number.doubleValue());
    }

    /**
     * Convert from a <code>BigDecimal</code>.
     * 
     * @return A string.
     * @param number
     *            The number to conver as a <code>BigDecimal</code>
     */
    public static String fmtFromBigDecimal(final BigDecimal number) {
        return fmtFromBigDecimal(number, Locale.getDefault());
    }

    /**
     * Returns the formatted string representation of a boolean.
     * 
     * @param bool
     *            The boolean value
     * @return The formatted string representation
     */
    public static String fmtFromBoolean(final boolean bool) {
        return fmtFromBoolean(bool);
    }

    /**
     * Convert from a <code>byte</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>byte</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromByte(final byte num, final Locale loc) {
        return NumberFormat.getNumberInstance(loc).format(num);
    }

    /**
     * Convert from a <code>byte</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>byte</code> to convert.
     */
    public static String fmtFromByte(final byte num) {
        return fmtFromByte(num, Locale.getDefault());
    }

    /**
     * Convert from a <code>char</code>.
     * 
     * @return A string.
     * @param chr
     *            The <code>char</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromChar(final char chr, final Locale loc) {
        return NumberFormat.getNumberInstance(loc).format(chr);
    }

    /**
     * Convert from a <code>char</code>.
     * 
     * @return A string.
     * @param chr
     *            The <code>char</code> to convert.
     */
    public static String fmtFromChar(final char chr) {
        return fmtFromChar(chr, Locale.getDefault());
    }

    /**
     * Returns the mapped date format item.
     * 
     * @param loc
     *            The locale.
     * @return The date format item.
     */
    private static FormatItem getDateFormatItem(final Locale loc) {
        FormatItem item = getDateFormatMap().get(loc);
        if (item == null) {
            SimpleDateFormat simpleFormat =
                (SimpleDateFormat) DateFormat.getDateInstance(
                    DateFormat.SHORT, loc);
            String pattern =
                getNormalizedPattern(simpleFormat.toPattern());
            simpleFormat.applyPattern(pattern);
            item = new FormatItem();
            item.format = simpleFormat;
            item.pattern = pattern;
            getDateFormatMap().put(loc, item);
        }
        return item;
    }

    /**
     * Returns the mapped time format item.
     * 
     * @param loc
     *            The locale.
     * @return The time format item.
     */
    private static FormatItem getTimeFormatItem(final Locale loc) {
        FormatItem item = getTimeFormatMap().get(loc);
        if (item == null) {
            SimpleDateFormat simpleFormat =
                (SimpleDateFormat) DateFormat.getTimeInstance(
                    DateFormat.MEDIUM, loc);
            String pattern =
                getNormalizedPattern(simpleFormat.toPattern());
            simpleFormat.applyPattern(pattern);
            item = new FormatItem();
            item.format = simpleFormat;
            item.pattern = pattern;
            getTimeFormatMap().put(loc, item);
        }
        return item;
    }

    /**
     * Returns the mapped timestamp format item.
     * 
     * @param loc
     *            The locale.
     * @return The timestamp format item.
     */
    private static FormatItem getTimestampFormatItem(final Locale loc) {
        FormatItem item = getTimestampFormatMap().get(loc);
        if (item == null) {
            SimpleDateFormat simpleFormat =
                (SimpleDateFormat) DateFormat.getDateTimeInstance(
                    DateFormat.SHORT, DateFormat.MEDIUM, loc);
            String pattern =
                getNormalizedPattern(simpleFormat.toPattern());
            simpleFormat.applyPattern(pattern);
            item = new FormatItem();
            item.format = simpleFormat;
            item.pattern = pattern;
            getTimestampFormatMap().put(loc, item);
        }
        return item;
    }

    /**
     * Returns the date format map.
     * 
     * @return The date format map.
     */
    private static HashMap<Locale, FormatItem> getDateFormatMap() {
        if (dateFormatMap == null) {
            dateFormatMap = new HashMap<Locale, FormatItem>();
        }
        return dateFormatMap;
    }

    /**
     * Returns the time format map.
     * 
     * @return The time format map.
     */
    private static HashMap<Locale, FormatItem> getTimeFormatMap() {
        if (timeFormatMap == null) {
            timeFormatMap = new HashMap<Locale, FormatItem>();
        }
        return timeFormatMap;
    }

    /**
     * Returns the timestamp format map.
     * 
     * @return The timestamp format map.
     */
    private static HashMap<Locale, FormatItem> getTimestampFormatMap() {
        if (timestampFormatMap == null) {
            timestampFormatMap = new HashMap<Locale, FormatItem>();
        }
        return timestampFormatMap;
    }

    /**
     * Returns the localized date format.
     * 
     * @param loc
     *            The locale.
     * @return The localized date format.
     */
    public static SimpleDateFormat getDateFormat(final Locale loc) {
        return getDateFormatItem(loc).format;
    }

    /**
     * Returns the localized time format.
     * 
     * @param loc
     *            The locale.
     * @return The localized time format.
     */
    public static SimpleDateFormat getTimeFormat(final Locale loc) {
        return getTimeFormatItem(loc).format;
    }

    /**
     * Returns the localized timestamp format.
     * 
     * @param loc
     *            The locale.
     * @return The localized timestamp format.
     */
    public static SimpleDateFormat getTimestampFormat(final Locale loc) {
        return getTimestampFormatItem(loc).format;
    }

    /**
     * Returns the localized pattern.
     * 
     * @param loc
     *            The locale.
     * @return The localized pattern.
     */
    public static String getNormalizedDatePattern(final Locale loc) {
        return getDateFormatItem(loc).pattern;
    }

    /**
     * Returns the localized pattern.
     * 
     * @param loc
     *            The locale.
     * @return The localized pattern.
     */
    public static String getNormalizedTimePattern(final Locale loc) {
        return getTimeFormatItem(loc).pattern;
    }

    /**
     * Returns the localized pattern.
     * 
     * @param loc
     *            The locale.
     * @return The localized pattern.
     */
    public static String getNormalizedTimestampPattern(final Locale loc) {
        return getTimestampFormatItem(loc).pattern;
    }

    /**
     * Convert from a <code>Date</code>.
     * 
     * @return A string.
     * @param date
     *            The <code>Date</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromDate(final java.sql.Date date,
            final Locale loc) {
        if (date == null) {
            return "";
        }
        return getDateFormat(loc).format(date);
    }

    /**
     * Convert from a <code>Date</code>.
     * 
     * @return A string.
     * @param date
     *            The <code>Date</code> to convert.
     */
    public static String fmtFromDate(final java.sql.Date date) {
        return fmtFromDate(date, Locale.getDefault());
    }

    /**
     * Convert from a <code>double</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>double</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromDouble(final double num, final Locale loc) {
        return java.text.NumberFormat.getNumberInstance(loc).format(num);
    }

    /**
     * Convert from a <code>double</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>double</code> to convert.
     */
    public static String fmtFromDouble(final double num) {
        return fmtFromDouble(num, Locale.getDefault());
    }

    /**
     * Convert from a <code>float</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>float</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromFloat(final float num, final Locale loc) {
        return java.text.NumberFormat.getNumberInstance(loc).format(num);
    }

    /**
     * Convert from a <code>float</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>float</code> to convert.
     */
    public static String fmtFromFloat(final float num) {
        return fmtFromFloat(num, Locale.getDefault());
    }

    /**
     * Convert from an <code>int</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>int</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromInt(final int num, final Locale loc) {
        return java.text.NumberFormat.getNumberInstance(loc).format(num);
    }

    /**
     * Convert from an <code>int</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>int</code> to convert.
     */
    public static String fmtFromInt(final int num) {
        return fmtFromInt(num, Locale.getDefault());
    }

    /**
     * Convert from a <code>long</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>long</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromLong(final long num, final Locale loc) {
        return java.text.NumberFormat.getNumberInstance(loc).format(num);
    }

    /**
     * Convert from a <code>long</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>long</code> to convert.
     */
    public static String fmtFromLong(final long num) {
        return fmtFromLong(num, Locale.getDefault());
    }

    /**
     * Convert from a <code>short</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>short</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromShort(final short num, final Locale loc) {
        return java.text.NumberFormat.getNumberInstance(loc).format(num);
    }

    /**
     * Convert from a <code>short</code>.
     * 
     * @return A string.
     * @param num
     *            The <code>short</code> to convert.
     */
    public static String fmtFromShort(final short num) {
        return fmtFromShort(num, Locale.getDefault());
    }

    /**
     * Convert from a <code>Time</code>.
     * 
     * @return A string.
     * @param time
     *            The <code>Time</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromTime(final java.sql.Time time,
            final Locale loc) {
        if (time == null) {
            return "";
        }
        return getTimeFormat(loc).format(time);
    }

    /**
     * Convert from a <code>Time</code>.
     * 
     * @return A string.
     * @param time
     *            The <code>Time</code> to convert.
     */
    public static String fmtFromTime(final java.sql.Time time) {
        return fmtFromTime(time, Locale.getDefault());
    }

    /**
     * Convert from a <code>Timestamp</code>.
     * 
     * @return A string.
     * @param time
     *            The <code>Timestamp</code> to convert.
     * @param loc
     *            The locale to apply.
     */
    public static String fmtFromTimestamp(final java.sql.Timestamp time,
            final Locale loc) {
        if (time == null) {
            return "";
        }
        return getTimestampFormat(loc).format(time);
    }

    /**
     * Convert from a <code>Timestamp</code>.
     * 
     * @return A string.
     * @param time
     *            The <code>Timestamp</code> to convert.
     */
    public static String fmtFromTimestamp(final java.sql.Timestamp time) {
        return fmtFromTimestamp(time, Locale.getDefault());
    }

    /**
     * Convert to <code>BigDecimal</code> from a formatted string.
     * 
     * @return A <code>BigDecimal</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     * @throws ParseException
     */
    public static BigDecimal fmtToBigDecimal(String str, final Locale loc)
            throws ParseException {
        if (str.length() == 0) {
            str = "0";
        }
        return new BigDecimal(java.text.NumberFormat
            .getNumberInstance(loc).parse(str).doubleValue());
    }

    /**
     * Convert to <code>BigDecimal</code> from a formatted string.
     * 
     * @return A <code>BigDecimal</code>
     * @param str
     *            The formatted string to convert.
     * @throws ParseException
     */
    public static BigDecimal fmtToBigDecimal(final String str)
            throws ParseException {
        return fmtToBigDecimal(str, Locale.getDefault());
    }

    /**
     * Convert to <code>boolean</code> from a formatted string.
     * 
     * @return A <code>boolean</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     */
    public static boolean fmtToBoolean(final String str, final Locale loc) {
        if (loc.getLanguage().equalsIgnoreCase("es")) {
            return (str.equalsIgnoreCase("Si") ? true : false);
        }
        if (loc.getLanguage().equalsIgnoreCase("fr")) {
            return (str.equalsIgnoreCase("Oui") ? true : false);
        }
        if (loc.getLanguage().equalsIgnoreCase("pt")) {
            return (str.equalsIgnoreCase("Sim") ? true : false);
        }
        if (loc.getLanguage().equalsIgnoreCase("de")) {
            return (str.equalsIgnoreCase("Ja") ? true : false);
        }
        if (loc.getLanguage().equalsIgnoreCase("pl")) {
            return (str.equalsIgnoreCase("Tak") ? true : false);
        }
        return (str.equalsIgnoreCase("Yes") ? true : false);
    }

    /**
     * Convert to <code>boolean</code> from a formatted string.
     * 
     * @return A <code>boolean</code>
     * @param str
     *            The formatted string to convert.
     */
    public static boolean fmtToBoolean(final String str) {
        return fmtToBoolean(str, Locale.getDefault());
    }

    /**
     * Convert to <code>byte</code> from a formatted string.
     * 
     * @return A <code>byte</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     * @throws ParseException
     */
    public static byte fmtToByte(String str, final Locale loc)
            throws ParseException {
        if (str.length() == 0) {
            str = "0";
        }
        return NumberFormat.getNumberInstance(loc).parse(str).byteValue();
    }

    /**
     * Convert to <code>byte</code> from a formatted string.
     * 
     * @return A <code>byte</code>
     * @param str
     *            The formatted string to convert.
     * @throws ParseException
     */
    public static byte fmtToByte(final String str) throws ParseException {
        return fmtToByte(str, Locale.getDefault());
    }

    /**
     * Convert to <code>char</code> from a formatted string.
     * 
     * @return A <code>char</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     * @throws ParseException
     */
    public static char fmtToChar(String str, final Locale loc)
            throws ParseException {
        if (str.length() == 0) {
            str = "0";
        }
        return (char) java.text.NumberFormat.getNumberInstance(loc)
            .parse(str).intValue();
    }

    /**
     * Convert to <code>char</code> from a formatted string.
     * 
     * @return A <code>char</code>
     * @param str
     *            The formatted string to convert.
     * @throws ParseException
     */
    public static char fmtToChar(final String str) throws ParseException {
        return fmtToChar(str, Locale.getDefault());
    }

    /**
     * Convert to <code>Date</code> from a formatted string, using the
     * normalized pattern.
     * 
     * @return A <code>Date</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     * @throws ParseException
     */
    public static Date fmtToDate(final String str, final Locale loc)
            throws ParseException {
        try {
            if (str == null || str.trim().length() == 0) {
                return null;
            }
            java.util.Date date =
                new SimpleDateFormat(getNormalizedDatePattern(loc))
                    .parse(str);
            return new Date(date.getTime());
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Convert to <code>Date</code> from a formatted string.
     * 
     * @return A <code>Date</code>
     * @param str
     *            The formatted string to convert.
     * @throws ParseException
     */
    public static Date fmtToDate(final String str) throws ParseException {
        return fmtToDate(str, Locale.getDefault());
    }

    /**
     * Convert to <code>double</code> from a formatted string.
     * 
     * @return A <code>double</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     * @throws ParseException
     */
    public static double fmtToDouble(String str, final Locale loc)
            throws ParseException {
        if (str.length() == 0) {
            str = "0";
        }
        return java.text.NumberFormat.getNumberInstance(loc).parse(str)
            .doubleValue();
    }

    /**
     * Convert to <code>double</code> from a formatted string.
     * 
     * @return A <code>double</code>
     * @param str
     *            The formatted string to convert.
     * @throws ParseException
     */
    public static double fmtToDouble(final String str)
            throws ParseException {
        return fmtToDouble(str, Locale.getDefault());
    }

    /**
     * Convert to <code>float</code> from a formatted string.
     * 
     * @return A <code>float</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     * @throws ParseException
     */
    public static float fmtToFloat(String str, final Locale loc)
            throws ParseException {
        if (str.length() == 0) {
            str = "0";
        }
        return java.text.NumberFormat.getNumberInstance(loc).parse(str)
            .floatValue();
    }

    /**
     * Convert to <code>float</code> from a formatted string.
     * 
     * @return A <code>float</code>
     * @param str
     *            The formatted string to convert.
     * @throws ParseException
     */
    public static float fmtToFloat(final String str) throws ParseException {
        return fmtToFloat(str, Locale.getDefault());
    }

    /**
     * Convert to <code>int</code> from a formatted string.
     * 
     * @return An <code>int</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     * @throws ParseException
     */
    public static int fmtToInt(String str, final Locale loc)
            throws ParseException {
        if (str.length() == 0) {
            str = "0";
        }
        return java.text.NumberFormat.getNumberInstance(loc).parse(str)
            .intValue();
    }

    /**
     * Convert to <code>int</code> from a formatted string.
     * 
     * @return An <code>int</code>
     * @param str
     *            The formatted string to convert.
     * @throws ParseException
     */
    public static int fmtToInt(final String str) throws ParseException {
        return fmtToInt(str, Locale.getDefault());
    }

    /**
     * Convert to <code>long</code> from a formatted string.
     * 
     * @return A <code>long</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     * @throws ParseException
     */
    public static long fmtToLong(String str, final Locale loc)
            throws ParseException {
        if (str.length() == 0) {
            str = "0";
        }
        return java.text.NumberFormat.getNumberInstance(loc).parse(str)
            .longValue();
    }

    /**
     * Convert to <code>long</code> from a formatted string.
     * 
     * @return A <code>long</code>
     * @param str
     *            The formatted string to convert.
     * @throws ParseException
     */
    public static long fmtToLong(final String str) throws ParseException {
        return fmtToLong(str, Locale.getDefault());
    }

    /**
     * Convert to <code>short</code> from a formatted string.
     * 
     * @return A <code>short</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     * @throws ParseException
     */
    public static short fmtToShort(String str, final Locale loc)
            throws ParseException {
        if (str.length() == 0) {
            str = "0";
        }
        return java.text.NumberFormat.getNumberInstance(loc).parse(str)
            .shortValue();
    }

    /**
     * Convert to <code>short</code> from a formatted string.
     * 
     * @return A <code>short</code>
     * @param str
     *            The formatted string to convert.
     * @throws ParseException
     */
    public static short fmtToShort(final String str) throws ParseException {
        return fmtToShort(str, Locale.getDefault());
    }

    /**
     * Convert to <code>Time</code> using the given pattern.
     * 
     * @return A <code>Time</code>
     * @param str
     *            The formatted string to convert.
     * @param timePattern
     *            The Time pattern to apply.
     */
    public static Time fmtToTime(final String str, final String timePattern) {
        String time = str;
        if ((time == null) || (time.trim().length() == 0)) {
            return null;
        }
        if (time.length() == 5) {
            time = addSecondsText(time);
        }
        try {
            java.util.Date date =
                new SimpleDateFormat(timePattern).parse(time);
            return new Time(date.getTime());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Convert to <code>Time</code> from a formatted string (##:##:##), using
     * the normalized pattern.
     * 
     * @return A <code>Time</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     */
    public static Time fmtToTime(final String str, final Locale loc) {
        return fmtToTime(str, getNormalizedTimePattern(loc));
        //
        // if (str == null) return null;
        // if (str.length() == 0) return null;
        // StringBuffer buf = new StringBuffer(str);
        // String fmt = "00:00:00";
        // if (buf.length() < 8) {
        // buf.append(fmt.substring(buf.length()));
        // }
        // String shour = buf.substring(0,2).trim();
        // String smin = buf.substring(3,5).trim();
        // String ssec = buf.substring(6,8).trim();
        // if (shour.length() == 0) return null;
        // if (smin.length() == 0) return null;
        // if (ssec.length() == 0) return null;
        // long hour = Long.parseLong(shour);
        // long min = Long.parseLong(smin);
        // long sec = Long.parseLong(ssec);
        // long time = (-(3600*1000)) + (3600*1000*hour) + (60*1000*min) +
        // (1000*sec);
        // return new Time(time);
    }

    /**
     * Convert to <code>Time</code> from a formatted string.
     * 
     * @return A <code>Time</code>
     * @param str
     *            The formatted string to convert.
     */
    public static Time fmtToTime(final String str) {
        return fmtToTime(str, Locale.getDefault());
    }

    private static String addSecondsText(final String str) {
        String text = str;
        char sep = 0;
        for (int i = text.length() - 1; i >= 0; i--) {
            if (!Character.isDigit(text.charAt(i))) {
                sep = text.charAt(i);
                break;
            }
        }
        if (sep > 0) {
            text += sep + "00";
        }
        return text;
    }

    /**
     * Convert to <code>Timestamp</code> using the given Timestamp pattern.
     * 
     * @return A <code>Timestamp</code>
     * @param str
     *            The formatted string to convert.
     * @param timestampPattern
     *            The Timestamp pattern to apply.
     */
    public static Timestamp fmtToTimestamp(final String str,
            final String timestampPattern) {
        String time = str;
        if ((time == null) || (time.trim().length() == 0)) {
            return null;
        }
        if (time.length() == 16) {
            time = addSecondsText(time);
        }
        try {
            java.util.Date date =
                new SimpleDateFormat(timestampPattern).parse(time);
            return new Timestamp(date.getTime());
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Convert to <code>Timestamp</code> from a formatted string, using the
     * normalized pattern.
     * 
     * @return A <code>Timestamp</code>
     * @param str
     *            The formatted string to convert.
     * @param loc
     *            The locale to apply.
     */
    public static Timestamp fmtToTimestamp(final String str,
            final Locale loc) {
        return fmtToTimestamp(str, getNormalizedTimestampPattern(loc));
    }

    /**
     * Convert to <code>Timestamp</code> from a formatted string.
     * 
     * @return A <code>Timestamp</code>
     * @param str
     *            The formatted string to convert.
     */
    public static Timestamp fmtToTimestamp(final String str) {
        return fmtToTimestamp(str, Locale.getDefault());
    }

    /**
     * Returns the normalized pattern for a date or time. The normalized pattern
     * is always two digit long for days and months and four digit long for the
     * year.
     * 
     * @return The normalized pattern.
     * @param pattern
     *            The original pattern.
     */
    public static String getNormalizedPattern(final String pattern) {
        StringBuffer buffer = new StringBuffer();
        int index = 0;
        int length = pattern.length();
        while (index < length) {
            char c = pattern.charAt(index);
            char origChar = c;
            if (c == 'a') {
                index++;
                continue;
            }
            if (c == 'k' || c == 'K' || c == 'h') {
                c = 'H';
            }
            if (c == 'd' || c == 'M' || c == 'y' || c == 'H' || c == 'm'
                || c == 's') {
                switch (c) {
                case 'd':
                    buffer.append("dd");
                    break;
                case 'M':
                    buffer.append("MM");
                    break;
                case 'y':
                    buffer.append("yyyy");
                    break;
                case 'H':
                    buffer.append("HH");
                    break;
                case 'm':
                    buffer.append("mm");
                    break;
                case 's':
                    buffer.append("ss");
                    break;
                }
                while (index < length && pattern.charAt(index) == origChar) {
                    index++;
                }
                continue;
            }
            buffer.append(c);
            index++;
        }
        return buffer.toString().trim();
    }

    /**
     * Convert a <code>BigDecimal</code> to the unformatted form.
     * 
     * @return A string.
     * @param n
     *            A <code>BigDecimal</code>.
     */
    public static String unfFromBigDecimal(final BigDecimal n) {
        if (n == null) {
            return "";
        }
        return n.toString();
    }

    /**
     * Convert a <code>boolean</code> to the unformatted form (true/false).
     * 
     * @return A string.
     * @param b
     *            A <code>boolean</code>.
     */
    public static String unfFromBoolean(final boolean b) {
        return (b ? Boolean.TRUE.toString() : Boolean.FALSE.toString());
    }

    /**
     * Convert a <code>byte</code> to the unformatted form.
     * 
     * @return A string.
     * @param n
     *            A <code>byte</code>.
     */
    public static String unfFromByte(final byte n) {
        return Byte.toString(n);
    }

    /**
     * Convert a <code>char</code> to the unformatted form.
     * 
     * @return A string.
     * @param n
     *            A <code>char</code>.
     */
    public static String unfFromChar(final char n) {
        return (new Character(n)).toString();
    }

    /**
     * Convert a <code>Date</code> to the unformatted form.
     * 
     * @return A string.
     * @param d
     *            A <code>Date</code>.
     */
    public static String unfFromDate(final java.sql.Date d) {
        if (d == null) {
            return "";
        }
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat ef = new SimpleDateFormat("G");
        String sdate = df.format(d);
        String sera = ef.format(d);
        return (sera.equals("BC") ? "-" + sdate : sdate);
    }

    /**
     * Convert a <code>double</code> to the unformatted form.
     * 
     * @return A string.
     * @param n
     *            A <code>double</code>.
     */
    public static String unfFromDouble(final double n) {
        return Double.toString(n);
    }

    /**
     * Convert a <code>float</code> to the unformatted form.
     * 
     * @return A string.
     * @param n
     *            A <code>float</code>.
     */
    public static String unfFromFloat(final float n) {
        return Float.toString(n);
    }

    /**
     * Convert an <code>int</code> to the unformatted form.
     * 
     * @return A string.
     * @param n
     *            An <code>int</code>.
     */
    public static String unfFromInt(final int n) {
        return Integer.toString(n);
    }

    /**
     * Convert a <code>long</code> to the unformatted form.
     * 
     * @return A string.
     * @param n
     *            A <code>long</code>.
     */
    public static String unfFromLong(final long n) {
        return Long.toString(n);
    }

    /**
     * Convert a <code>short</code> to the unformatted form.
     * 
     * @return A string.
     * @param n
     *            A <code>short</code>.
     */
    public static String unfFromShort(final short n) {
        return Short.toString(n);
    }

    /**
     * Convert from a <code>Time</code> to an unlocalized string with the format
     * <code>hhmmss</code>.
     * 
     * @return The string.
     * @param t
     *            A <code>Time</code>.
     */
    public static String unfFromTime(final java.sql.Time t) {
        return unfFromTime(t, false);
    }

    /**
     * Convert from a <code>Time</code> to an unlocalized string with the format
     * <code>hhmmss</code> or <code>hhmmssnnn</code>.
     * 
     * @return The string.
     * @param time
     *            A <code>Time</code>.
     * @param millis
     *            A <code>boolean</code> to include milliseconds.
     */
    public static String unfFromTime(final java.sql.Time time,
            final boolean millis) {
        if (time == null) {
            return "";
        }
        StringBuffer pattern = new StringBuffer();
        pattern.append("HHmmss");
        if (millis) {
            pattern.append("SSS");
        }
        SimpleDateFormat df = new SimpleDateFormat(pattern.toString());
        return df.format(time);
    }

    /**
     * Convert from a <code>Timestamp</code> to an unlocalized string with the
     * format <b>yyyymmddhhmmss</b>.
     * 
     * @return The string.
     * @param t
     *            A <code>Timestamp</code>.
     */
    public static String unfFromTimestamp(final java.sql.Timestamp t) {
        return unfFromTimestamp(t, true, false);
    }

    /**
     * Convert from a <code>Timestamp</code> to an unlocalized string with the
     * format <code>yyyymmddhhmmss</code> or <code>yyyymmddhhmmssnnn</code>
     * 
     * @return The string.
     * @param timestamp
     *            A <code>Timestamp</code>.
     * @param millis
     *            A <code>boolean</code> to include milliseconds.
     */
    public static String unfFromTimestamp(
            final java.sql.Timestamp timestamp, final boolean millis) {
        return unfFromTimestamp(timestamp, millis, false);
    }

    /**
     * Convert from a <code>Timestamp</code> to an unlocalized string with the
     * format <code>yyyymmddhhmmss</code> or <code>yyyymmddhhmmssnnn</code>
     * 
     * @return The string.
     * @param timestamp
     *            A <code>Timestamp</code>.
     * @param millis
     *            A <code>boolean</code> to include milliseconds.
     * @param separators
     *            A boolean to include standard separators
     */
    public static String unfFromTimestamp(
            final java.sql.Timestamp timestamp, final boolean millis,
            final boolean separators) {
        if (timestamp == null) {
            return "";
        }
        StringBuffer pattern = new StringBuffer();
        pattern.append("yyyy");
        if (separators) {
            pattern.append("-");
        }
        pattern.append("MM");
        if (separators) {
            pattern.append("-");
        }
        pattern.append("dd");
        if (separators) {
            pattern.append(" ");
        }
        pattern.append("HH");
        if (separators) {
            pattern.append(":");
        }
        pattern.append("mm");
        if (separators) {
            pattern.append(":");
        }
        pattern.append("ss");
        if (millis) {
            if (separators) {
                pattern.append(".");
            }
            pattern.append("SSS");
        }
        SimpleDateFormat df = new SimpleDateFormat(pattern.toString());
        return df.format(timestamp);
    }

    /**
     * Timestamp Pattern (yyyyMMdd-HHmmss-SSS)
     */
    public final static String TIMESTAMP_SUFFIX_PATTERN =
        "yyyyMMdd-HHmmss-SSS";

    /**
     * Convert from a <code>Timestamp</code> to an unlocalized string with the
     * format <code>yyyymmddhhmmss</code> or <code>yyyymmddhhmmssnnn</code>
     * 
     * @return The string.
     * @param timestamp
     *            A <code>Timestamp</code>.
     */
    public static String unfFromTimestampSuffix(
            final java.sql.Timestamp timestamp) {
        if (timestamp == null) {
            return "";
        }
        StringBuffer pattern = new StringBuffer();
        pattern.append(TIMESTAMP_SUFFIX_PATTERN);
        SimpleDateFormat df = new SimpleDateFormat(pattern.toString());
        return df.format(timestamp);
    }

    /**
     * Convert a <code>Timestamp</code> from an string with the format
     * <code>yyyyMMdd-HHmmss-SSS</code> of a timesatmp suffix.
     * 
     * @return The <code>Timestamp</code>.
     * @param str
     *            The string to convert.
     */
    public static Timestamp unfToTimestampSuffix(final String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            int year = Integer.parseInt(str.substring(0, 4));
            int month = Integer.parseInt(str.substring(4, 6));
            int day = Integer.parseInt(str.substring(6, 8));
            int hour = Integer.parseInt(str.substring(9, 11));
            int min = Integer.parseInt(str.substring(11, 13));
            int sec = Integer.parseInt(str.substring(13, 15));
            int msec = Integer.parseInt(str.substring(16, 19));
            return Calendar.createTimestamp(year, month, day, hour, min,
                sec, msec);
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid unf timestamp "
                + str);
        }
    }

    /**
     * Convert an unformatted string to <code>BigDecimal</code>.
     * 
     * @return A <code>BigDecimal</code>
     * @param str
     *            The string to convert.
     */
    public static BigDecimal unfToBigDecimal(String str) {
        if (str == null || str.length() == 0) {
            str = "0";
        }
        BigDecimal b = new BigDecimal(str);
        return b.setScale(b.scale(), BigDecimal.ROUND_HALF_UP);
    }

    /**
     * Convert a unformatted string to a <code>boolean</code>.
     * 
     * @return A <code>boolean</code>.
     * @param str
     *            The string to convert.
     */
    public static boolean unfToBoolean(final String str) {
        return (str.equals("true") ? true : false);
    }

    /**
     * Convert an unformatted string to <code>byte</code>.
     * 
     * @return A <code>byte</code>
     * @param str
     *            The string to convert.
     */
    public static byte unfToByte(final String str) {
        try {
            return Byte.valueOf(str).byteValue();
        } catch (Exception e) {
            return (byte) 0;
        }
    }

    /**
     * Convert an unformatted string to <code>char</code>.
     * 
     * @return A <code>char</code>
     * @param str
     *            The string to convert.
     */
    public static char unfToChar(final String str) {
        try {
            return (char) Integer.valueOf(str).intValue();
        } catch (Exception e) {
            return (char) 0;
        }
    }

    /**
     * Convert an unformatted string to <code>Date</code>.
     * 
     * @return A <code>Date</code>
     * @param str
     *            The string to convert.
     */
    public static Date unfToDate(final String str) {
        try {
            if (str.trim().length() == 0) {
                return null;
            }
            java.text.SimpleDateFormat fmt =
                new java.text.SimpleDateFormat("yyyyMMdd");
            return new Date(fmt.parse(str).getTime());
        } catch (Exception e) {
            throw new IllegalArgumentException(
                "Invalid unf date (yyyyMMdd) " + str);
        }
    }

    /**
     * Convert an unformatted string to <code>double</code>.
     * 
     * @return A <code>double</code>
     * @param str
     *            The string to convert.
     */
    public static double unfToDouble(final String str) {
        try {
            return Double.valueOf(str).doubleValue();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Convert an unformatted string to <code>float</code>.
     * 
     * @return A <code>float</code>
     * @param str
     *            The string to convert.
     */
    public static float unfToFloat(final String str) {
        try {
            return Float.valueOf(str).floatValue();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Convert an unformatted string to <code>int</code>.
     * 
     * @return An <code>int</code>
     * @param str
     *            The string to convert.
     */
    public static int unfToInt(final String str) {
        try {
            return Integer.valueOf(str).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Convert an unformatted string to <code>long</code>.
     * 
     * @return A <code>long</code>
     * @param str
     *            The string to convert.
     */
    public static long unfToLong(final String str) {
        try {
            return Long.valueOf(str).longValue();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Convert an unformatted string to <code>short</code>.
     * 
     * @return A <code>short</code>
     * @param str
     *            The string to convert.
     */
    public static short unfToShort(final String str) {
        try {
            return Short.valueOf(str).shortValue();
        } catch (Exception e) {
            return (short) 0;
        }
    }

    /**
     * Convert a <code>Time</code> from an unlocalized string with the format
     * <code>hhmmss</code>.
     * 
     * @return The <code>Time</code>.
     * @param str
     *            The string to convert.
     */
    public static Time unfToTime(final String str) {
        return unfToTime(str, false);
    }

    /**
     * Convert a <code>Time</code> from an unlocalized string with the format
     * <code>hhmmss</code> or <code>hhmmssnnn</code>.
     * 
     * @return The <code>Time</code>.
     * @param str
     *            The string to convert.
     * @param millis
     *            A <code>boolean</code> indicating if the string contains
     *            millisecond data.
     */
    public static Time unfToTime(final String str, final boolean millis) {
        try {
            if (str.trim().length() == 0) {
                return null;
            }
            int hour = Integer.parseInt(str.substring(0, 2));
            int min = Integer.parseInt(str.substring(2, 4));
            if (millis) {
                int sec = Integer.parseInt(str.substring(4, 6));
                int msec = Integer.parseInt(str.substring(6, 9));
                return Calendar.createTime(hour, min, sec, msec);
            } else {
                int sec = Integer.parseInt(str.substring(4));
                return Calendar.createTime(hour, min, sec);
            }
        } catch (Exception e) {
            if (millis) {
                throw new IllegalArgumentException(
                    "Invalid unf time (hhmmssnnn) " + str);
            } else {
                throw new IllegalArgumentException(
                    "Invalid unf time (hhmmss) " + str);
            }
        }
    }

    /**
     * Convert a <code>Timestamp</code> from an unlocalized string with the
     * format <code>yyyymmddhhmmss</code> or <code>yyyymmddhhmmssnnn</code> or
     * <code>yyyy-mm-dd hh:mm:ss</code> or <code>yyyy-mm-dd hh:mm:ss.nnn</code>.
     * 
     * @return The <code>Timestamp</code>.
     * @param str
     *            The string to convert.
     */
    public static Timestamp unfToTimestamp(final String str) {
        if (str == null || str.length() == 0) {
            return null;
        }
        try {
            if (str.length() == 23) {
                int year = Integer.parseInt(str.substring(0, 4));
                int month = Integer.parseInt(str.substring(5, 7));
                int day = Integer.parseInt(str.substring(8, 10));
                int hour = Integer.parseInt(str.substring(11, 13));
                int min = Integer.parseInt(str.substring(14, 16));
                int sec = Integer.parseInt(str.substring(17, 19));
                int msec = Integer.parseInt(str.substring(20, 23));
                return Calendar.createTimestamp(year, month, day, hour,
                    min, sec, msec);
            } else if (str.length() == 19) {
                int year = Integer.parseInt(str.substring(0, 4));
                int month = Integer.parseInt(str.substring(5, 7));
                int day = Integer.parseInt(str.substring(8, 10));
                int hour = Integer.parseInt(str.substring(11, 13));
                int min = Integer.parseInt(str.substring(14, 16));
                int sec = Integer.parseInt(str.substring(17, 19));
                return Calendar.createTimestamp(year, month, day, hour,
                    min, sec);
            } else if (str.length() == 17) {
                int year = Integer.parseInt(str.substring(0, 4));
                int month = Integer.parseInt(str.substring(4, 6));
                int day = Integer.parseInt(str.substring(6, 8));
                int hour = Integer.parseInt(str.substring(8, 10));
                int min = Integer.parseInt(str.substring(10, 12));
                int sec = Integer.parseInt(str.substring(12, 14));
                int msec = Integer.parseInt(str.substring(14, 17));
                return Calendar.createTimestamp(year, month, day, hour,
                    min, sec, msec);
            } else if (str.length() == 14) {
                int year = Integer.parseInt(str.substring(0, 4));
                int month = Integer.parseInt(str.substring(4, 6));
                int day = Integer.parseInt(str.substring(6, 8));
                int hour = Integer.parseInt(str.substring(8, 10));
                int min = Integer.parseInt(str.substring(10, 12));
                int sec = Integer.parseInt(str.substring(12, 14));
                return Calendar.createTimestamp(year, month, day, hour,
                    min, sec);
            } else {
                throw new Exception();
            }
        } catch (Exception e) {
            throw new IllegalArgumentException("Invalid unf timestamp "
                + str);
        }
    }

    /**
     * Converts unicodes to encoded \\uxxxx and writes out any of the characters
     * in specialSaveChars with a preceding slash
     * 
     * @param theString
     *            The string to convert.
     * @return a string.
     */
    public static String toEncoded(final String theString) {
        if (theString == null) {
            return new String();
        }
        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len * 2);

        for (int x = 0; x < len;) {
            aChar = theString.charAt(x++);
            switch (aChar) {
            case '\\':
                outBuffer.append('\\');
                outBuffer.append('\\');
                continue;
            case '\t':
                outBuffer.append('\\');
                outBuffer.append('t');
                continue;
            case '\n':
                outBuffer.append('\\');
                outBuffer.append('n');
                continue;
            case '\r':
                outBuffer.append('\\');
                outBuffer.append('r');
                continue;
            case '\f':
                outBuffer.append('\\');
                outBuffer.append('f');
                continue;
            default:
                if ((aChar < 20) || (aChar > 127)) {
                    outBuffer.append('\\');
                    outBuffer.append('u');
                    outBuffer.append(toHex((aChar >> 12) & 0xF));
                    outBuffer.append(toHex((aChar >> 8) & 0xF));
                    outBuffer.append(toHex((aChar >> 4) & 0xF));
                    outBuffer.append(toHex((aChar >> 0) & 0xF));
                } else {
                    if (specialSaveChars.indexOf(aChar) != -1) {
                        outBuffer.append('\\');
                    }
                    outBuffer.append(aChar);
                }
            }
        }
        return outBuffer.toString();
    }

    /**
     * Converts encoded \\uxxxx to unicode chars and changes special saved chars
     * to their original forms
     * 
     * @param theString
     *            The string to convert.
     * @return a string.
     */
    public static String toUnicode(final String theString) {
        char aChar;
        int len = theString.length();
        StringBuffer outBuffer = new StringBuffer(len);

        for (int x = 0; x < len;) {
            aChar = theString.charAt(x++);
            if (aChar == '\\') {
                aChar = theString.charAt(x++);
                if (aChar == 'u') {
                    // Read the xxxx
                    int value = 0;
                    for (int i = 0; i < 4; i++) {
                        aChar = theString.charAt(x++);
                        switch (aChar) {
                        case '0':
                        case '1':
                        case '2':
                        case '3':
                        case '4':
                        case '5':
                        case '6':
                        case '7':
                        case '8':
                        case '9':
                            value = (value << 4) + aChar - '0';
                            break;
                        case 'a':
                        case 'b':
                        case 'c':
                        case 'd':
                        case 'e':
                        case 'f':
                            value = (value << 4) + 10 + aChar - 'a';
                            break;
                        case 'A':
                        case 'B':
                        case 'C':
                        case 'D':
                        case 'E':
                        case 'F':
                            value = (value << 4) + 10 + aChar - 'A';
                            break;
                        default:
                            throw new IllegalArgumentException(
                                "Malformed \\uxxxx encoding.");
                        }
                    }
                    outBuffer.append((char) value);
                } else {
                    if (aChar == 't') {
                        aChar = '\t';
                    } else if (aChar == 'r') {
                        aChar = '\r';
                    } else if (aChar == 'n') {
                        aChar = '\n';
                    } else if (aChar == 'f') {
                        aChar = '\f';
                    }
                    outBuffer.append(aChar);
                }
            } else {
                outBuffer.append(aChar);
            }
        }
        return outBuffer.toString();
    }

    /**
     * Convert a nibble to a hex character
     * 
     * @param nibble
     *            the nibble to convert.
     * @return a char
     */
    public static char toHex(final int nibble) {
        return hexDigit[(nibble & 0xF)];
    }
}
