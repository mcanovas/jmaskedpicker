package org.sk.maskedpicker;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Locale;

/**
 * A timestamp formatter that acts as expected.
 * 
 * @author Mario Cánovas
 */
public class TimestampFormatter extends MaskFormatter {
    private static final long serialVersionUID = 1L;

    /** The locale. */
    private Locale locale = null;

    /**
     * Default constructor.
     */
    public TimestampFormatter() {
        this(Locale.getDefault(), true);
    }

    /**
     * Default constructor.
     * 
     * @param editSeconds
     *            A boolean to indicate that seconds must be included or not.
     */
    public TimestampFormatter(final boolean editSeconds) {
        this(Locale.getDefault(), editSeconds);
    }

    /**
     * Constructor assigning the locale.
     * 
     * @param locale
     *            The locale to use.
     * @param editSeconds
     *            A boolean to indicate that seconds must be included or not.
     */
    public TimestampFormatter(final Locale locale,
            final boolean editSeconds) {
        super();
        this.locale = locale;
        String mask =
            getTimeMask(Convert.getNormalizedTimestampPattern(locale));
        if (!editSeconds) {
            mask = mask.substring(0, mask.length() - 3);
        }
        setMask(mask);
    }

    /**
     * Convert the string to a value.
     */
    @Override
    public Object stringToValue(final String value) throws ParseException {
        return Convert.fmtToTimestamp(value, locale);
    }

    /**
     * Convert the string to a Timestamp using the given pattern.
     * 
     * @param value
     *            The string value
     * @param timestampPattern
     *            The pattern to be used
     * @return A Timestamp representation of the given value.
     * @throws ParseException
     */
    public Timestamp stringToValue(final String value,
            final String timestampPattern) throws ParseException {
        return Convert.fmtToTimestamp(value, timestampPattern);
    }

    /**
     * Convert the value to a string.
     */
    @Override
    public String valueToString(final Object value) throws ParseException {
        String str = Convert.fmtFromTimestamp((Timestamp) value, locale);
        if (str.trim().length() == 0) {
            return super.valueToString(value);
        }
        return str;
    }

    /**
     * Returns the mask that this mask formatter must use.
     * 
     * @return The mask.
     */
    private String getTimeMask(final String datePattern) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < datePattern.length(); i++) {
            char c = datePattern.charAt(i);
            switch (c) {
            case 'd':
            case 'M':
            case 'y':
            case 'H':
            case 'm':
            case 's':
                buffer.append("#");
                break;
            default:
                buffer.append(c);
                break;
            }
        }
        return buffer.toString();
    }
}
