package org.sk.maskedpicker;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.ParseException;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.Icon;
import javax.swing.JPanel;
import javax.swing.JWindow;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

/**
 * Compound control to manage dates with a calendar panel.
 * 
 * @author Mario Cánovas
 */
public class JFieldPickerDate extends JFieldPickerButton {

    /**
     * Serial version UID
     */
    private static final long serialVersionUID = 1L;

    /**
     * Event handler.
     */
    class EventHandler implements PropertyChangeListener,
            AncestorListener, KeyListener {
        /*
         * (non-Javadoc)
         * @see java.beans.PropertyChangeListener#propertyChange(java.beans.
         * PropertyChangeEvent)
         */
        public void propertyChange(final PropertyChangeEvent event) {
            handlePropertyChange(event);
        }

        /*
         * (non-Javadoc)
         * @see
         * javax.swing.event.AncestorListener#ancestorAdded(javax.swing.event
         * .AncestorEvent)
         */
        public void ancestorAdded(final AncestorEvent event) {
            handleAncestor(event);
        }

        /*
         * (non-Javadoc)
         * @see
         * javax.swing.event.AncestorListener#ancestorMoved(javax.swing.event
         * .AncestorEvent)
         */
        public void ancestorMoved(final AncestorEvent event) {
            handleAncestor(event);
        }

        /*
         * (non-Javadoc)
         * @see
         * javax.swing.event.AncestorListener#ancestorRemoved(javax.swing.event
         * .AncestorEvent)
         */
        public void ancestorRemoved(final AncestorEvent event) {
            handleAncestor(event);
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.KeyListener#keyPressed(java.awt.event.KeyEvent)
         */
        public void keyPressed(final KeyEvent e) {
            handleKey(e);
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.KeyListener#keyReleased(java.awt.event.KeyEvent)
         */
        public void keyReleased(final KeyEvent e) {
            handleKey(e);
        }

        /*
         * (non-Javadoc)
         * @see java.awt.event.KeyListener#keyTyped(java.awt.event.KeyEvent)
         */
        public void keyTyped(final KeyEvent e) {
            handleKey(e);
        }
    }

    private final EventHandler eventHandler = new EventHandler();

    /**
     * Event handler.
     */
    class ButtonAction extends AbstractAction {
        private static final long serialVersionUID = 1L;

        /*
         * (non-Javadoc)
         * @see
         * java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent
         * )
         */
        public void actionPerformed(final ActionEvent e) {
            if (e.getSource() == getButton()) {
                if (getWindowCalendar().isVisible()) {
                    closeWindowCalendar();
                } else {
                    if (!(getContext().getDate() == null)) {
                        getPanelCalendar().setCurrentDate(
                            getContext().getDate());
                    } else {
                        try {
                            getPanelCalendar().setCurrentDate(
                                Convert.fmtToDate(JFieldPickerDate.this
                                    .getValue()));
                        } catch (ParseException e1) {
                            e1.printStackTrace();
                        }
                    }
                    getPanelCalendar().configure();
                    openWindowCalendar();
                }
            }
            if (e.getSource() == getTextField()) {
                getPanelCalendar().setCurrentDate(getContext().getDate());
                getPanelCalendar().configure();
            }
        }
    }

    /** Calendar pane. */
    private JPanelCalendar panelCalendar = null;

    /** Calendar window. */
    private JWindow windowCalendar = null;

    /**
     * Default constructor.
     */
    public JFieldPickerDate() {
        this(new IconArrow());
    }

    public JFieldPickerDate(final Icon icon) {
        super(icon);
        setContext(new FieldPikerContext());
        getButton().addAncestorListener(eventHandler);
    }

    /**
     * Set this component context and apply the field label.
     * 
     * @param context
     *            The component context.
     */
    @Override
    public void setContext(final FieldPikerContext context) {
        context.setActionLookup(new ButtonAction());
        super.setContext(context);
    }

    /**
     * Returns the calendar panel.
     * 
     * @return The calendar panel
     */
    public JPanelCalendar getPanelCalendar() {
        return getPanelCalendar(null);
    }

    /**
     * If the current {@link JPanelCalendar} is null, it creates one using the
     * given {@link Font}.
     * 
     * @param font
     *            The {@link Font} to be used
     * @return The calendar panel
     */
    public JPanelCalendar getPanelCalendar(final Font font) {
        if (panelCalendar == null) {
            if (font != null) {
                panelCalendar = new JPanelCalendar(font);
            } else {
                panelCalendar = new JPanelCalendar();
            }

            panelCalendar.setName("PaneCalendarInCombo");
            panelCalendar.setBorder(BorderFactory
                .createRaisedBevelBorder());
            panelCalendar.addPropertyChangeListener(eventHandler);
            panelCalendar.setCurrentDate(new Date());
            panelCalendar.configure();
            Component[] components =
                SwingUtil.getAllComponents(panelCalendar);
            for (int i = 0; i < components.length; i++) {
                components[i].addKeyListener(eventHandler);
            }
        }
        return panelCalendar;
    }

    /**
     * Returns the calendar window.
     * 
     * @return The calendar window.
     */
    public JWindow getWindowCalendar() {
        if (windowCalendar == null) {
            Window owner = null;
            Component parent = this;
            while (parent != null) {
                if (parent instanceof Window) {
                    owner = (Window) parent;
                    break;
                }
                if (parent.getParent() == parent) {
                    break;
                }
                parent = parent.getParent();
            }
            windowCalendar = new JWindow(owner);
            windowCalendar.setName("WindowCalendar");
            // windowCalendar.setLayout(new GridBagLayout());

            JPanel contentPane = new JPanel(new GridBagLayout());
            contentPane.setName("JWindowCalendarContentPane");
            GridBagConstraints constraints = new GridBagConstraints();
            constraints.gridx = 0;
            constraints.gridy = 0;
            constraints.fill = GridBagConstraints.BOTH;
            constraints.anchor = GridBagConstraints.CENTER;
            contentPane.add(getPanelCalendar(), constraints);

            windowCalendar.setContentPane(contentPane);

            Dimension d = new Dimension(200, 200);
            windowCalendar.setSize(d);
        }
        return windowCalendar;
    }

    /**
     * Handles the event.
     */
    private void handleKey(final KeyEvent e) {
        if (e.getID() == KeyEvent.KEY_PRESSED
            && e.getKeyCode() == KeyEvent.VK_ESCAPE) {
            if (getWindowCalendar().isVisible()) {
                closeWindowCalendar();
            }
        }
    }

    /**
     * Handles the event.
     */
    private void handleAncestor(final AncestorEvent e) {
        if (e.getSource() == getButton()) {
            if (getWindowCalendar().isVisible()) {
                openWindowCalendar();
            }
        }
    }

    /**
     * Handles the event.
     */
    private void handlePropertyChange(final PropertyChangeEvent e) {
        if (e.getSource() == getPanelCalendar()) {
            if (e.getNewValue() == null) {
            }
        }
        if (e.getPropertyName() == JPanelCalendar.DATE_CLICKED) {
            setDate(getPanelCalendar().getCurrentDate());
            closeWindowCalendar();
        }
    }

    /**
     * Opens the calendar window.
     */
    protected void openWindowCalendar() {
        requestFocusInWindow();
        getWindowCalendar().pack();
        double butHeight = getButton().getBounds().getHeight();

        double scrHeight = SwingUtil.getScreenSize().getHeight();
        double scrWidth = SwingUtil.getScreenSize().getWidth();
        double wndHeight = getWindowCalendar().getSize().getHeight();
        double wndWidth = getWindowCalendar().getSize().getWidth();

        Point pt = getButton().getLocationOnScreen();

        // By default left/down the button.
        double y = pt.getY() + butHeight;
        double x = pt.getX();

        if (x + wndWidth > scrWidth) {
            x = scrWidth - wndWidth;
        }
        if (x < 0) {
            x = 0;
        }
        if (y + wndHeight > scrHeight) {
            y = scrHeight - wndHeight;
        }
        if (y < 0) {
            y = 0;
        }
        getWindowCalendar().setLocation((int) x, (int) y);

        getWindowCalendar().pack();
        getWindowCalendar().setVisible(true);
        Icon icon = getButton().getIcon();
        if (icon instanceof IconArrow) {
            ((IconArrow) icon).setDirection(IconArrow.NORTH);
        }
        getButton().invalidate();
        getButton().repaint();
    }

    /**
     * Closes the calendar window.
     */
    protected void closeWindowCalendar() {
        Icon icon = getButton().getIcon();
        if (icon instanceof IconArrow) {
            ((IconArrow) icon).setDirection(IconArrow.SOUTH);
        }
        getButton().invalidate();
        getButton().repaint();
        getWindowCalendar().setVisible(false);
        requestFocusInWindow();
    }

    public void setDate(final Date date) {
        getPanelCalendar().setCurrentDate(date);
        getPanelCalendar().configure();
        super
            .setValue(Convert.fmtFromDate(date, getContext().getLocale()));
    }

    public Date getDate() {
        Date fmtToDate;
        try {
            fmtToDate = Convert.fmtToDate(getTextField().getValue());
        } catch (ParseException e) {
            e.printStackTrace();
            fmtToDate = null;
        }
        return fmtToDate;
    }
}
