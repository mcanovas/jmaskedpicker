package org.sk.maskedpicker;

import java.sql.Time;
import java.text.ParseException;
import java.util.Locale;

/**
 * A time formatter that acts as expected.
 * 
 * @author Mario Cánovas
 */
public class TimeFormatter extends MaskFormatter {
    private static final long serialVersionUID = 1L;

    /** The locale. */
    private Locale locale = null;

    /**
     * Default constructor.
     */
    public TimeFormatter() {
        this(Locale.getDefault(), true);
    }

    /**
     * Default constructor.
     * 
     * @param editSeconds
     *            A boolean to indicate that seconds must be included or not.
     */
    public TimeFormatter(final boolean editSeconds) {
        this(Locale.getDefault(), editSeconds);
    }

    /**
     * Constructor assigning the locale.
     * 
     * @param locale
     *            The locale to use.
     * @param editSeconds
     *            A boolean to indicate that seconds must be included or not.
     */
    public TimeFormatter(final Locale locale, final boolean editSeconds) {
        super();
        this.locale = locale;
        String mask =
            getTimeMask(Convert.getNormalizedTimePattern(locale));
        if (!editSeconds) {
            mask = mask.substring(0, mask.length() - 3);
        }
        setMask(mask);
    }

    /**
     * Convert the string to a Time using the given pattern.
     * 
     * @param value
     *            The string value
     * @param timePattern
     *            The pattern to be used
     * @return A Time representation of the given value.
     * @throws ParseException
     */
    public Time stringToValue(final String value, final String timePattern)
            throws ParseException {
        return Convert.fmtToTime(value, timePattern);
    }

    /**
     * Convert the string to a value.
     */
    @Override
    public Object stringToValue(final String value) throws ParseException {
        return Convert.fmtToTime(value, locale);
    }

    /**
     * Convert the value to a string.
     */
    @Override
    public String valueToString(final Object value) throws ParseException {
        String str = Convert.fmtFromTime((Time) value, locale);
        if (str.trim().length() == 0) {
            return super.valueToString(value);
        }
        return str;
    }

    /**
     * Returns the mask that this mask formatter must use.
     * 
     * @return The mask.
     */
    private String getTimeMask(final String datePattern) {
        StringBuffer buffer = new StringBuffer();
        for (int i = 0; i < datePattern.length(); i++) {
            char c = datePattern.charAt(i);
            switch (c) {
            case 'd':
            case 'M':
            case 'y':
            case 'H':
            case 'm':
            case 's':
                buffer.append("#");
                break;
            default:
                buffer.append(c);
                break;
            }
        }
        return buffer.toString();
    }
}
