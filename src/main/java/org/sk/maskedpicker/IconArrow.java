package org.sk.maskedpicker;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

import javax.swing.Icon;
import javax.swing.UIManager;

/**
 * An icon used in masked field button component.
 * 
 * @author Mario Cánovas
 */
public class IconArrow implements Icon {

    /**
	 * 
	 */
    public final static int NORTH = 0;

    /**
	 * 
	 */
    public final static int SOUTH = 1;

    /**
	 * 
	 */
    public final static int WEST = 2;

    /**
	 * 
	 */
    public final static int EAST = 3;

    private final int size = 9;

    private int direction = SOUTH;

    /**
     * Set the arrow direction.
     * 
     * @param direction
     *            The direction of the arrow.
     */
    public void setDirection(final int direction) {
        this.direction = direction;
    }

    /**
     * Return the icon size.
     * 
     * @return The size.
     */
    public int getIconSize() {
        return size;
    }

    /**
     * @see javax.swing.Icon#getIconHeight()
     */
    public int getIconHeight() {
        return size;
    }

    /**
     * @see javax.swing.Icon#getIconWidth()
     */
    public int getIconWidth() {
        return size;
    }

    /**
     * @see javax.swing.Icon#paintIcon(java.awt.Component, java.awt.Graphics,
     *      int, int)
     */
    public void paintIcon(final Component c, final Graphics g,
            final int x, final int y) {

        boolean enabled = c.isEnabled();
        Color oldColor = g.getColor();
        int mid, i, j;
        j = 0;
        int size = Math.max(getIconSize() - 4, 2);
        mid = (size / 2) + 1;

        g.translate(x, y);
        if (enabled) {
            g.setColor(Color.black);
        } else {
            g.setColor(UIManager.getColor("controlShadow"));
        }

        switch (direction) {
        case NORTH:
            for (i = 0; i < size; i++) {
                g.drawLine(mid - i, i, mid + i, i);
            }
            if (!enabled) {
                g.setColor(UIManager.getColor("controlLtHighlight"));
                g.drawLine(mid - i + 2, i, mid + i, i);
            }
            break;
        case SOUTH:
            if (!enabled) {
                g.translate(1, 1);
                g.setColor(UIManager.getColor("controlLtHighlight"));
                for (i = size - 1; i >= 0; i--) {
                    g.drawLine(mid - i, j, mid + i, j);
                    j++;
                }
                g.translate(-1, -1);
                g.setColor(UIManager.getColor("controlShadow"));
            }

            j = 0;
            for (i = size - 1; i >= 0; i--) {
                g.drawLine(mid - i, j, mid + i, j);
                j++;
            }
            break;
        case WEST:
            for (i = 0; i < size; i++) {
                g.drawLine(i, mid - i, i, mid + i);
            }
            if (!enabled) {
                g.setColor(UIManager.getColor("controlLtHighlight"));
                g.drawLine(i, mid - i + 2, i, mid + i);
            }
            break;
        case EAST:
            if (!enabled) {
                g.translate(1, 1);
                g.setColor(UIManager.getColor("controlLtHighlight"));
                for (i = size - 1; i >= 0; i--) {
                    g.drawLine(j, mid - i, j, mid + i);
                    j++;
                }
                g.translate(-1, -1);
                g.setColor(UIManager.getColor("controlShadow"));
            }

            j = 0;
            for (i = size - 1; i >= 0; i--) {
                g.drawLine(j, mid - i, j, mid + i);
                j++;
            }
            break;
        }
        g.translate(-x, -y);
        g.setColor(oldColor);
    }
}
